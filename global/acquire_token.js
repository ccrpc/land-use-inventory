const msal = require("@azure/msal-node");

const msalConfig = {
    auth: {
        clientId: "d8d18bd3-4193-4daf-afc3-0ce80d959df4",
        authority: "https://login.microsoftonline.com/0a412f90-1a58-4884-832c-6dfab7f8f376",
        clientSecret: "95ev5WOB22lDWft7~_Tn5y~b~4~YEHchaB",
    }
};
 
const pca = new msal.PublicClientApplication(msalConfig);

const deviceCodeRequest = {
    deviceCodeCallback: (response) => (console.log(response.message)),
    scopes: ["Files.ReadWrite.All"],
    timeout: 200,
};

pca.acquireTokenByDeviceCode(deviceCodeRequest).then((response) => {
    process.stdout.write(response.accessToken);
}).catch((error) => {
    process.stderr.write(JSON.stringify(error));
});
