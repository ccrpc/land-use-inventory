const pool = require('./config');

async function getVideos() {
  const client = await pool.connect();
  let result = await client.query(`
    select distinct video
    from land_use.frame
    order by video;
  `);
  await client.release(true);
  process.stdout.write(result.rows.map((row) => row.video).join('\n'));
}

getVideos();
