const cliProgress = require('cli-progress');
const os = require('os');
const path = require('path');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const pool = require('./config');
const stitchFrame = require('./stitchFrame');

async function getCount() {
  const client = await pool.connect();
  let result = await client.query(`
    select count(*) as remaining
    from land_use.frame 
    where cts is distinct from null
      and target_photo_location_id is distinct from null
      and qa_status is not distinct from null
    group by true;
  `);
  await client.release(true);
  return (result.rows.length) ? parseInt(result.rows[0].remaining) : 0;
}

async function getBatch() {
  const client = await pool.connect();
  let result = await client.query(`
    select id, 
      target_photo_location_id || '_' || replace(substr(timestamp::varchar, 0, 11), '-', '') || '.jpg' as filename,
      frame,
      video
    from land_use.frame
    where target_photo_location_id is distinct from null
      and qa_status is not distinct from null
    order by video,
      frame
    limit 1000;
  `);
  await client.release(true);
  return result.rows;
}

async function markExtracted(id) {
  const client = await pool.connect();
  let result = await client.query(`
    update land_use.frame
    set qa_status = 'Extracted'
    where id = ${id};
  `);
  await client.release(true);
}

const streamsCache = {};
async function getStreams(video) {
  if (streamsCache.hasOwnProperty(video)) return streamsCache[video];
  let streamsCmd = `ffprobe ${video} 2>&1`;
  let res = await exec(streamsCmd);
  let matches = Array.from(res.stdout.matchAll(/#(\d):(\d)\(eng\): Video: hevc/g));
  streamsCache[video] = matches.map((match) => `${match[1]}:${match[2]}`);
  return streamsCache[video];
}

async function extractFrame(videoPath, stream, frame) {
  const seconds = Math.floor(parseFloat(frame) / 29.97 * 1000) / 1000;
  const extractCmd = `ffmpeg \
    -ss ${seconds} \
    -i ${videoPath} \
    -map ${stream} \
    -vsync 0 \
    -vframes 1 \
    -c:v png \
    -f image2pipe -`;
  let res = await exec(extractCmd, {encoding: 'buffer', maxBuffer: 15000*1024});
  return res.stdout;
}

async function addBatch(queue, progress, callback) {
  let batch = await getBatch();
  if (!batch.length) callback();

  for (let frame of batch) {
    let videoPath = path.join(process.env.PROCESSED_VIDEOS_PATH, frame.video);
    let imagePath = path.join(
      process.env.EXTRACTED_IMAGES_PATH, frame.filename);
    let streams = await getStreams(videoPath);
    queue.add(async () => {
      await stitchFrame(
        extractFrame(videoPath, streams[0], frame.frame),
        extractFrame(videoPath, streams[1], frame.frame),
        imagePath);
      await markExtracted(frame.id);
      progress.increment();
    });
  }
}

async function extractFrames() {
  const pQueue =  await import('p-queue');
  let queue = new pQueue.default({concurrency: os.cpus().length});

  let total = await getCount();
  const progress = new cliProgress.SingleBar(
    {}, cliProgress.Presets.shades_classic);
  progress.start(total, 0);

  return new Promise(async (resolve) => {
    const callback = () => {
      progress.stop();
      resolve();
    };
    await addBatch(queue, progress, callback);
    queue.on('idle', async () => await addBatch(queue, progress, callback));
  });
}

extractFrames();