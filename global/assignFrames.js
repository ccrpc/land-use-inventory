const path = require('path');
const pool = require('./config');


async function assignFrames() {
  const client = await pool.connect();
  await client.query(`
    with matches as (
      select distinct on (frame.id)
        frame.id as frame_id,
        date(frame.timestamp) as frame_date,
        st_distance(frame.geom, tpl.geom) as distance,
        tpl.id as tpl_id,
        tpl.segment_id
      from land_use.frame as frame
      inner join land_use.target_photo_location as tpl
      on st_dwithin(frame.geom, tpl.geom, 35)
        and tpl.high_priority
        and frame.gps_valid
      order by frame.id,
        st_distance(frame.geom, tpl.geom)
    ),
    tpl_matches as (
      select distinct on (tpl_id, frame_date)
        *
      from matches
      order by tpl_id,
        frame_date,
        distance
    ),
    seg_matches as (
      select segment_id,
      frame_date,
      avg(distance) as avg_distance,
      count(*) as match_count
      from tpl_matches
      group by segment_id,
        frame_date
    ),
    seg_tpl as (
      select segment_id,
        count(*) as tpl_count
      from land_use.target_photo_location as tpl
      where high_priority
      group by segment_id
    ),
    seg_match_pct as (
      select seg_tpl.segment_id,
        seg_matches.frame_date,
        seg_matches.avg_distance,
        seg_matches.match_count::double precision /
          seg_tpl.tpl_count::double precision as pct,
        seg_tpl.tpl_count - seg_matches.match_count as missing
      from seg_tpl
      inner join seg_matches
      on seg_tpl.segment_id = seg_matches.segment_id
    )
    update land_use.frame as frame
    set target_photo_location_id = matches.tpl_id
    from tpl_matches as matches
    inner join seg_match_pct as pct
      on matches.segment_id = pct.segment_id
        and matches.frame_date = pct.frame_date
    where frame.id = matches.frame_id
      and (pct.pct >= 0.75 or pct.missing <= 2)
      and frame.target_photo_location_id is not distinct from null;
  `);
  await client.release(true);
}

assignFrames();
