const sharp = require('sharp');


function extractFaceSide(original, offset) {
  return original.clone().extract({
    left: offset,
    top: 0,
    width: 688,
    height: 1344
  });
}

async function stitchFace(original, leftOffset, rightOffset) {
  const [left, right] = await Promise.all([
    extractFaceSide(original, leftOffset),
    extractFaceSide(original, rightOffset)
  ]);

  const combined = await left
    .extend({right: 688 - 64})
    .composite([{
      input: await right.toBuffer(),
      gravity: 'northeast'
    }])
    .toBuffer();
  
    // We have to start a new pipeline after composite().
    // See: https://github.com/lovell/sharp/issues/1635
  return sharp(combined)
    .resize({
      height: 1344,
      width: 1344,
      fit: 'fill'
    })
    .toBuffer();
}

async function stitchRow(bufferPromise) {
  const original = sharp(await bufferPromise);
  const [leftFace, rightFace] = await Promise.all([
    stitchFace(original, 0, 688),
    stitchFace(original, 4096 - (688 * 2), 4096 - 688)
  ]);

  return original
    .extract({
      left: 32,
      top: 0,
      width: 1344 * 3,
      height: 1344
    })
    .composite([{
      input: leftFace,
      gravity: 'northwest'
    }, {
      input: rightFace,
      gravity: 'northeast'
    }
  ]).toBuffer();
}

async function stitchFrame(frontPromise, backPromise, outPath) {
  const [front, back] = await Promise.all([
    stitchRow(frontPromise),
    stitchRow(backPromise)
  ]);

  await sharp(front)
    .extend({bottom: 1344})
    .composite([{
      input: back,
      gravity: 'southeast'
    }])
    .jpeg({
      quality: 75
    })
    .toFile(outPath);
}

module.exports = stitchFrame;
