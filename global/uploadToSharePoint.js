const fs = require('fs');
const request = require('request');


function uploadImageToSharepoint(framesDir, drive, file, token, callback) {
  let data = fs.readFileSync(`${framesDir}/${drive}/${file}`);
  //The approach to upload image file by its name using Drive/Drive-Items API.
  var option1 = {
    url: `https://graph.microsoft.com/v1.0/sites/champaigncountyillinois.sharepoint.com,c2ddb8c3-c73e-4623-a2c7-2e975b97ed7a,62a56b88-4353-4f37-bc80-7c2f761dadff/drives/b!w7jdwj7HI0aixy6XW5fteohrpWJTQzdPvIB8L3Ydrf9r0FJSGdu5Qo5k4WD63wOM/root:/${file}:/content`, 
    method: 'PUT',
    body: data,
    headers: {
      'Content-Type': 'image/jpg',
      Authorization: 'Bearer ' + token
    },
  };

  request(option1, function (err, res, body) {
    if (err) return callback(err, body, res);
    if (parseInt(res.statusCode / 100, 10) !== 2) {
        if (body.error) {
            return callback(new Error(res.statusCode + ': ' + (body.error.message || body.error)), body, res);
        }
        return callback(err, body, res);
    }
    callback(err, body ,res);
  });

  //The approach to update metadata by using List/List-Items API.
  // var option2 ={
  //   url: `https://graph.microsoft.com/v1.0/sites/champaigncountyillinois.sharepoint.com,c2ddb8c3-c73e-4623-a2c7-2e975b97ed7a,62a56b88-4353-4f37-bc80-7c2f761dadff/lists/5252d06b-db19-42b9-8e64-e160fadf038c/items/{item-id}`, 
  //   method: 'PATCH',
  //   body: JSON.stringify({
  //     "x": 2,
  //     "y": 1
  //   }),
  //   headers: {
  //     'Content-Type': 'application/json',
  //     Authorization: 'Bearer ' + token
  //   },
  // };

  // request(option2, function (err, res, body) {
  //   if (err) return callback(err, body, res);
  //   if (parseInt(res.statusCode / 100, 10) !== 2) {
  //       if (body.error) {
  //           return callback(new Error(res.statusCode + ': ' + (body.error.message || body.error)), body, res);
  //       }
  //       return callback(err, body, res);
  //   }
  //   callback(err, body ,res);   
  // });
}


module.exports.uploadToSharePoint = async (framesDir, token) => {
  console.log(framesDir);
  fs.readdir(framesDir, async (err, drives) => {
    for (drive of drives) {
      fs.readdir(`${framesDir}/${drive}`, async (err, files) => {
        for (file of files) {
          await uploadImageToSharepoint(framesDir, drive, file, token,
            function (err, body, res) {
              if (err) {
                console.error(err);
              } else {
                console.log(res['body']);
                console.log(JSON.parse(body));
              }
            });
        }
      });
    }
  });
};