const goproTelemetry = require('gopro-telemetry');
const path = require('path');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const pool = require('./config');


// Champaign County bounding box in WGS84 (degrees)
// TODO: Make these values configurable.
const bbox = [-88.463634, 39.879092, -87.92876, 40.400612];

function gpsInvalid(sampleGps) {
  let lon = sampleGps['GPS (Long.) [deg]'];
  let lat = sampleGps['GPS (Lat.) [deg]'];
  return (
    sampleGps.fix < 2 ||
    sampleGps.precision > 500 ||
    lon < bbox[0] ||
    lat < bbox[1] ||
    lon > bbox[2] ||
    lat > bbox[3]
  );
}

async function getStreams(videoPath) {
  let cmd = `ffprobe -show_streams -print_format json ${videoPath}`;
  let res = await exec(cmd);
  return JSON.parse(res.stdout).streams;
}

async function getMetadataTiming(videoPath, metadataStreamIndex) {
  let cmd = `ffprobe -show_packets -select_streams ${metadataStreamIndex} -print_format json ${videoPath}`;
  let res = await exec(cmd, {maxBuffer: 500*1024*1024});
  return JSON.parse(res.stdout).packets;
}

async function getMetadataStream(videoPath, metadataStreamIndex) {
  let cmd = `ffmpeg -i ${videoPath} -codec copy -map 0:${metadataStreamIndex} -f rawvideo -`;
  let res = await exec(cmd, {encoding: 'buffer', maxBuffer: 500*1024*1024});
  return res.stdout;
}

async function insertFrames(rows) {
  const client = await pool.connect();
  await client.query(`
    insert into land_use.frame (
      video,
      timestamp,
      frame,
      cts,
      gps_fix,
      gps_precision,
      gps_valid,
      temperature,
      altitude,
      speed_2d,
      speed_3d,
      accelerometer_x,
      accelerometer_y, 
      accelerometer_z,
      geom
    ) values ${rows.join(', ')}`);
  await client.release(true);
}

function getOrNull(obj, propName) {
  if (!obj) return 'null';
  return obj[propName] || 'null';
}

async function findFrames(videoPath) {
  let outDir = 'nogps';
  let streams = await getStreams(videoPath);
  let videoStream, audioStream, metadataStream;

  for (let stream of streams) {
    if (stream.codec_type === 'video') {
      videoStream = stream;
    } else if (stream.codec_type === 'audio') {
      audioStream = stream;
    } else if (stream.codec_tag_string === 'gpmd') {
      metadataStream = stream;
    }
  }

  let metadataTiming = await getMetadataTiming(videoPath, metadataStream.index);
  let videoDuration = parseFloat(videoStream.duration);

  const telemetry = await goproTelemetry({
    rawData: await getMetadataStream(videoPath, metadataStream.index),
    timing: {
      videoDuration: videoDuration,
      frameDuration: videoDuration / parseFloat(videoStream.nb_frames),
      start: metadataStream.tags.creation_time,
      samples: metadataTiming.map((packet) => {
        return {
          cts: packet.pts,
          duration: packet.duration
        };
      })
    }
  }, {
    stream: ['GPS5', 'ACCL'],
    repeatSticky: true,
    repeatHeaders: true,
    groupTimes: (audioStream) ? 'frames' : null
  });
  const { ACCL: accl, GPS5: gps } = telemetry['1']['streams'];

  const rows = gps['samples'].map((sampleGps, i) => {
    let sampleAccl = accl['samples'][i];

    // Store time as local time, not UTC.
    let timestamp = new Date(sampleGps['date']);
    let tzOffset = (timestamp.getTimezoneOffset() * 60000);
    let localISOTime = (new Date(timestamp - tzOffset))
      .toISOString().slice(0, -1);

    outDir = localISOTime.substr(0, 10).replace(/-/g, '');

    let gpsValid = !gpsInvalid(sampleGps);
    let geom = (gpsValid) ? `
      ST_Transform(
        ST_SetSRID(
          ST_MakePoint(${sampleGps['GPS (Long.) [deg]']},
          ${sampleGps['GPS (Lat.) [deg]']}),
        4326),
      3435)` : 'null';

    return `(
      '${outDir}/${path.basename(videoPath)}',
      '${localISOTime}',
      ${i},
      ${sampleGps['cts']},
      ${sampleGps['fix']},
      ${Math.round(sampleGps['precision'])},
      ${gpsValid},
      ${getOrNull(sampleAccl, 'temperature [°C]')},
      ${getOrNull(sampleGps, 'GPS (Alt.) [m]')},
      ${getOrNull(sampleGps, 'GPS (2D speed) [m/s]')},
      ${getOrNull(sampleGps, 'GPS (3D speed) [m/s]')},
      ${getOrNull(sampleAccl, 'Accelerometer (x) [m/s2]')},
      ${getOrNull(sampleAccl, 'Accelerometer (y) [m/s2]')},
      ${getOrNull(sampleAccl, 'Accelerometer (z) [m/s2]')},
      ${geom}
    )`;
  });
  if (rows.length) await insertFrames(rows);
  process.stdout.write(outDir);
}

findFrames(process.argv[2]);
