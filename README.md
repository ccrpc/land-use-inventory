	# Land Use Inventory

Repository for resources related to the land use inventory project.

Project [Wireframe](https://drive.google.com/file/d/1VnDfUqvSBvmNm7UK8m6a3nGu-ssvHprM/view?usp=sharing)

## Part 1: Video Extraction

## Start Point
**create run.sh file:**	
```
export DB_HOST=''	
export DB_PORT=''	
export DB_NAME=''	
export DB_USER=''	
export DB_PASSWORD=''	
node --max-old-space-size=16384 run.js ${path to the input videos. 
# default path: ~/gopro	
# default video path: ~/gopro/video/
```
## Create Routes Layer

- [ ] Create routes layer from the videos to record the collected area so far. 
## Video Processing

- [ ] Extract the video's stream and transform them to eac format photos.

- [ ] Insert these generated photos data into PostGIS database.
## Upload To SharePoint

- [ ] Our Azure [Land Use Inventory application](https://portal.azure.com/#blade/Microsoft_AAD_RegisteredApps/ApplicationMenuBlade/Overview/appId/d8d18bd3-4193-4daf-afc3-0ce80d959df4/isMSAApp/) site. We can add/remove required permissions here.
- [ ] Upload these photos with their metadata into [SharePoint](https://champaigncountyillinois.sharepoint.com/sites/landuse/360/Forms/AllItems.aspx?viewid=9e9c6860%2Dd9c9%2D4e19%2Db027%2D5230010105db)(metadata part not completed).
- [ ] Note that upload process should base on [Drive-Item API](https://docs.microsoft.com/en-us/graph/api/driveitem-put-content?view=graph-rest-1.0&tabs=http).
- [ ] Update metadata shold base on [List-Item API](https://docs.microsoft.com/en-us/sharepoint/dev/sp-add-ins/working-with-lists-and-list-items-with-rest). The useful testing urls of this List-Item API: [Overall Structure](https://docs.microsoft.com/en-us/graph/api/resources/sharepoint?view=graph-rest-1.0#sharepoint-api-root-resources).
```
tricky part of uploading process:
# the format we should use as the upload url is this one:
PUT /sites/{site-id}/drive/items/{parent-id}:/{filename}:/content

# However, it's not quite right based on the real implementation. The correct format should be:
PUT /sites/{site-id}/drives/{parent-id}/root:/{file_name}:/content
```

## Acquire Access Token

- [ ] The upload process may require an access token in order to get the permission to upload. It could be acquired by directly run acquire_token.js script, the response will contains an "access token".(by [device code approach](https://docs.microsoft.com/en-us/azure/active-directory/develop/scenario-desktop-acquire-token?tabs=nodejs#command-line-tool-without-a-web-browser))
