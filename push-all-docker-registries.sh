#!/bin/bash

if [ -z "$1" ]
  then
    echo 'No Argument Supplied'
    exit
fi

LABEL=$1

echo 'Pushing to repositories with label:' $LABEL

echo '======== Building Webapp ========'
docker build -t registry.gitlab.com/ccrpc/land-use-inventory/webapp:$LABEL /opt/code/land-use-inventory/webapp/
echo '======== Webapp Built ========'

echo '======== Pushing Webapp ========'
docker push registry.gitlab.com/ccrpc/land-use-inventory/webapp:$LABEL
echo '======== Webapp Pushed ========'

echo '======== Building trex ========'
docker build -t registry.gitlab.com/ccrpc/land-use-inventory/trex:$LABEL /opt/code/land-use-inventory/trex/
echo '======== trex Built ========'

echo '======== Pushing trex ========'
docker push registry.gitlab.com/ccrpc/land-use-inventory/trex:$LABEL
echo '======== trex Pushed ========'

echo '======== Building features-api ========'
docker build -t registry.gitlab.com/ccrpc/land-use-inventory/features:$LABEL /opt/code/land-use-inventory/features_api/
echo '======== features-api Built ========'

echo '======== Pushing features-api ========'
docker push registry.gitlab.com/ccrpc/land-use-inventory/features:$LABEL
echo '======== features-api Pushed ========'


echo '======== Building Viewer ========'
docker build -t registry.gitlab.com/ccrpc/land-use-inventory/viewer:$LABEL /opt/code/land-use-inventory/viewer/
echo 'Viewer Built'

echo '======== Pushing Viewer ========'
docker push registry.gitlab.com/ccrpc/land-use-inventory/viewer:$LABEL
echo '======== Viewer Pushed ========'
