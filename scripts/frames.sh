#!/bin/sh

export $(cat .env | xargs)

videos="$(/snap/bin/node --max-old-space-size=8192 ./src/getVideos.js)"

for videoPath in $PROCESSED_VIDEOS_PATH*/*.360
do
  videoFilename="$(basename $videoPath)"
  videoDir="$(basename $(dirname $videoPath))"
  videoName="$videoDir/$videoFilename"
  
  if (echo "$videos" | grep -xq "$videoName"); then
    echo "Skipping existing video $videoName..."
  else
    echo "Processing $videoName..."
    /snap/bin/node --max-old-space-size=8192 ./src/findFrames.js "$videoPath"
  fi
done
