#!/bin/bash

read -p "Enter token: " token

for path in ~/Pictures/*.jpg; do
    filename=`basename "$path"`
    echo ""
    echo "Uploading $filename..."
    curl -X PUT --silent --fail --show-error \
        -H "Content-Type: image/jpeg" \
        -H "Authorization: Bearer $token" \
        --upload-file "$path" \
        "https://graph.microsoft.com/v1.0/sites/champaigncountyillinois.sharepoint.com,c2ddb8c3-c73e-4623-a2c7-2e975b97ed7a,62a56b88-4353-4f37-bc80-7c2f761dadff/drives/b!w7jdwj7HI0aixy6XW5fteohrpWJTQzdPvIB8L3Ydrf9r0FJSGdu5Qo5k4WD63wOM/root:/$filename:/content" \
        && mv "$path" ~/Pictures/Uploaded || exit 1
done
