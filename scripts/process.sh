#!/bin/sh

export $(cat .env | xargs)

for videoPath in $HOME/Videos/*.360
do
  echo "Processing $videoPath..."
  echo "  - Finding frames..."
  outDir=`/snap/bin/node --max-old-space-size=8192 ./src/findFrames.js "$videoPath"`

  echo "  - Cleaning up..."
  outVideoPath="$PROCESSED_VIDEOS_PATH$outDir"
  mkdir -p "$outVideoPath"
  mv "$videoPath" "$outVideoPath"
done

echo "Assigning frames..."
/snap/bin/node --max-old-space-size=8192 ./src/assignFrames.js
