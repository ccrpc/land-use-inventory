#!/bin/sh

export $(cat .env | xargs)

/snap/bin/node --max-old-space-size=8192 ./src/extractFrames.js
