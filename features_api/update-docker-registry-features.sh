#!/bin/bash

docker build -t registry.gitlab.com/ccrpc/land-use-inventory/features:latest /opt/code/land-use-inventory/features_api/

docker push registry.gitlab.com/ccrpc/land-use-inventory/features:latest
