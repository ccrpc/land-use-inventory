# Features

REST API for PostGIS

## Configuration
```json
{
  "port": 8080,
  "users": {
    "transportation": {
      "type": "postgres",
      "auth": {
        "secret": "longrandomstring",
        "issuer": "example.com"
      },
      "options": {
        "user": "web",
        "host": "gis.example.com",
        "database": "mpo",
        "password": "abc123",
        "port": 5432,
        "ssl": {},
        "defaultSrid": 3435
      }
    }
  }
}
```

## Usage
```
TOKEN=`npm run --silent token generate transportation 604800`

npm start

curl -X GET http://localhost:8080/transportation/traffic.volumes_2015 \
     --header "Authorization: Bearer $TOKEN"
```
### GET REQUEST
You can either use GET to get the entire contents of a table or a specific feature on that table.
The format is \[server\]/:user/:table and optionally /:user/:table/:fid where fid is the primary key of the feature
you want to get from the database. You do not need to specify the name of the primary key as that will be obtained
from the database information_schema via SQL.

### PATCH REQUEST
You send a PATCH request when you wish to UPDATE a feature in the database, the format for this is similar to GET
for a specific feauture in that the url is \[server\]/:user/:table/:fid.
That said in the body of the PATCH request you must pass in json along the format of 
```json
{
    "properties": {
        "name_of_field_1": 6,
        "name_of_field_2": "{2300}"
    }
}
```

### POST REQUEST
You send a POST request when you wish to add a *new* field to the database.
More documentation pending.