docker build -t features:sh_test .
docker create -p 8080:8080 --name features_sh_test features:sh_test
docker cp config.json features_sh_test:/usr/src/app/config.json
docker start features_sh_test

docker exec features_sh_test yarn run --silent token generate lui_user 604800 | xclip

curl -X GET http://localhost:8080/lui_user/test.parcel --header "Authorization: Bearer $(xclip -o)"

docker stop features_sh_test
docker container rm features_sh_test
docker image rm features:sh_test