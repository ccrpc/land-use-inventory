#!/bin/bash

docker build -t features:sh_token .
docker create -p 8080:8080 --name features_sh_token features:sh_token yarn start /usr/src/app/config.json
docker cp config.json features_sh_token:/usr/src/app/config.json
docker start features_sh_token

docker exec features_sh_token yarn run --silent token generate lui_user 604800 | xclip

echo "Authorization: Bearer $(xclip -o)"

docker stop features_sh_token
docker container rm features_sh_token
docker image rm features:sh_token