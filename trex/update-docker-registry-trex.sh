#!/bin/bash

docker build -t registry.gitlab.com/ccrpc/land-use-inventory/trex:latest /opt/code/land-use-inventory/trex/

docker push registry.gitlab.com/ccrpc/land-use-inventory/trex:latest