WITH t1 as 
( 
	SELECT
		frame.id as f_id,
		st_azimuth(frame.geom, lead(frame.geom) OVER (PARTITION BY frame.video ORDER BY frame.id)) as trajectory
	FROM land_use.frame
	WHERE target_photo_location_id IS NOT NULL
)
UPDATE land_use.frame
SET vehicle_heading = t1.trajectory
FROM t1
WHERE frame.id = t1.f_id