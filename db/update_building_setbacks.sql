WITH frontline AS 
( 
	SELECT DISTINCT ON (building_id)
	building_id AS b_id,
	ST_ShortestLine(ST_centroid(building.geom), street.geom) AS geom
	FROM land_use.study_area_buildings AS building
	JOIN street.segment AS street ON ST_DWithin(building.geom, street.geom, 1000) 
	WHERE street_name = street.name
	ORDER BY building_id, ST_Length(st_shortestline(ST_Centroid(building.geom), street.geom)) 
),
p_original AS (
	SELECT
		id,
		ST_MakeLine(LAG((pt).geom, 1, NULL) OVER (PARTITION BY id ORDER BY id, (pt).path),(pt).geom) AS geom
	FROM (SELECT id, ST_DumpPoints(geom) AS pt FROM land_use.parcel) AS dumps
),
p_segments as(
SELECT 
	id, 
	unnest(geom_agg[2:]) as geom
FROM (
	SELECT DISTINCT ON (id) 
		id, 
		array_agg(geom) as geom_agg
	FROM p_original
	GROUP BY id) as p_agg
),
edge_skewer AS
(
	SELECT
		b_id,
		p.id AS p_id,
		ST_Makeline(
			ST_Startpoint(frontline.geom),
			ST_Translate(
				ST_Startpoint(frontline.geom),
				sin(ST_Azimuth(ST_Startpoint(frontline.geom), ST_Endpoint(frontline.geom))) * ST_Length(ST_LongestLine(p.geom, p.geom)),
				cos(ST_Azimuth(ST_Startpoint(frontline.geom), ST_Endpoint(frontline.geom))) * ST_Length(ST_LongestLine(p.geom, p.geom))
			)
		) AS geom
	FROM frontline
	JOIN land_use.building AS b ON b.building_id = b_id
	JOIN land_use.parcel AS p ON ST_Contains(p.geom, ST_Centroid(b.geom))
)
SELECT 
	b_id,
	-- Setback
	ST_ShortestLine(front_p_seg.geom, b.geom) AS front_setback,
	ST_ShortestLine(left_p_seg.geom, b.geom) AS left_setback,
	ST_ShortestLine(rear_p_seg.geom, b.geom) AS rear_setback,
	ST_ShortestLine(right_p_seg.geom, b.geom) AS right_setback,
	front_p_seg.geom <-> b.geom AS front_setback_length,
	left_p_seg.geom <-> b.geom AS left_setback_length,
	rear_p_seg.geom <-> b.geom AS rear_setback_length,
	right_p_seg.geom <-> b.geom AS right_setback_length
FROM edge_skewer
JOIN land_use.building AS b 
	ON edge_skewer.b_id = b.building_id
	AND ST_Intersects(edge_skewer.geom, b.geom)
JOIN p_segments AS front_p_seg
	ON edge_skewer.p_id = front_p_seg.id
	AND ST_Intersects(edge_skewer.geom, front_p_seg.geom)
JOIN p_segments AS right_p_seg
	ON edge_skewer.p_id = right_p_seg.id
	AND ST_Intersects(ST_Rotate(edge_skewer.geom, pi()/2, ST_Startpoint(edge_skewer.geom)), right_p_seg.geom)
JOIN p_segments AS rear_p_seg
	ON edge_skewer.p_id = rear_p_seg.id
	AND ST_Intersects(ST_Rotate(edge_skewer.geom, pi(), ST_Startpoint(edge_skewer.geom)), rear_p_seg.geom)
JOIN p_segments AS left_p_seg
	ON edge_skewer.p_id = left_p_seg.id
	AND ST_Intersects(ST_Rotate(edge_skewer.geom, 3*pi()/2, ST_Startpoint(edge_skewer.geom)), left_p_seg.geom)
