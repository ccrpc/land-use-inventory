INSERT INTO land_use.target_photo_location (geom, segment_id, pt_index)
With buffer as (
	SELECT ST_Buffer(ST_UNION(municipal.geom), 7920) as geom
	FROM boundary.municipal
	WHERE municipal.name = 'Champaign' or municipal.name = 'Urbana' or municipal.name = 'Savoy'
)
SELECT
 	point_segment,
	segment_id,
	pt_index
FROM
(
	SELECT ST_LineInterpolatePoint(
		lu_segment.geom,
		(first_pt_distance + 40 * (pt_index - 1)) / ST_Length(lu_segment.geom)
	  ) as point_segment,
	  lu_segment.segment_id,
	  pt_index
	FROM (
		SELECT segment.geom,
		  segment_id,
		  (ST_Length(segment.geom)::numeric % 40 / 2) as first_pt_distance,
		  (ST_Length(segment.geom)::numeric / 40)::int as total_point,
		  generate_series(
			1,
			(ST_Length(segment.geom)::numeric / 40) + 1::int
		  ) as pt_index
		FROM street.segment
	  ) as range
	JOIN street.segment as lu_segment ON range.segment_id = lu_segment.segment_id
) as points
INNER JOIN buffer ON ST_Contains(buffer.geom, points.point_segment)

