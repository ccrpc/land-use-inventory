-- Drop old tables
-- drop table if exists land_use_inventory.parcel cascade;
-- drop table if exists land_use_inventory.parcel_use cascade;
-- Create schema
-- create schema if not exists
-- land_use_inventory;
-- Rename columns
--- PHOTO
create table land_use.photo (
  id serial primary key,
  target_photo_location_id varchar(100),
  date_captured timestamp,
  from_video varchar(100),
  temperature numeric(7, 5),
  altitude numeric(6, 3),
  speed_2d_m_s numeric(4, 3),
  speed_3d_m_s numeric(4, 3),
  accelerometer_x_m_s2 numeric(20, 15),
  accelerometer_y_m_s2 numeric(20, 15),
  accelerometer_z_m_s2 numeric(20, 15),
  distance_to_target numeric(20, 15),
  car_heading numeric(20,15)
);
select AddGeometryColumn ('land_use', 'photo', 'geom', 3435, 'POINT', 2);
--- TARGET PHOTO LOCATION
create table land_use.target_photo_location (segment_id varchar(10), pt_index int);
select AddGeometryColumn (
    'land_use',
    'target_photo_location',
    'geom',
    3435,
    'POINT',
    2
  );
--- PARCEL
create table land_use.parcel (
  id serial primary key,
  fair_market_value numeric(10, 2),
  photo_id varchar(100),
  photo_orientation numeric(5, 2),
  area_sq_feet numeric(6, 2),
  impervious_area_sq_feet numeric(6, 2),
  building_coverage_area_sq_feet numeric(6, 2),
  mixed_use_sq_feet numeric(6, 2)
);
select AddGeometryColumn ('land_use', 'parcel', 'geom', 3435, 'POLYGON', 2);
create index sidx_parcel_geom
    on land_use.parcel using gist
    (geom)
    tablespace pg_default;
-- PACEL USE
create table land_use.parcel_use (
  parcel_use_id serial primary key,
  parcel_id int,
  lbcs_activity_code text,
  lbcs_site_code text,
  constraint fk_parcel_parcel_use foreign key(parcel_id) references land_use.parcel(id)
);
--- BUILDING
create table land_use.building (
  building_id serial primary key,
  year_built int,
  non_residential_rent numeric(10, 2),
  footprint_area_sq_ft numeric(10, 2),
  mixed_use numeric(10, 2),
  front_set_back_ft numeric(10, 2),
  side_set_back_ft numeric(10, 2),
  parcel_id int,
  constraint fk_parcel_building foreign key(parcel_id) references land_use.parcel(id)
);
select AddGeometryColumn ('land_use', 'building', 'geom', 3435, 'POLYGON', 2);
create index sidx_building_geom
    on land_use.building using gist
    (geom)
    tablespace pg_default;
--- BUILDING USE
create table land_use.building_use (
  building_use_id serial primary key,
  building_id int,
  lbcs_structure_code text,
  stories int,
  number_of_unit int,
  constraint fk_building_building_use foreign key(building_id) references land_use.building(building_id)
);
--- PARKING LOT
create table land_use.parking_lot (
  parking_lot_id serial primary key,
  parking_spaces int,
  area_sq_ft numeric(10, 2),
  parcel_id int,
  constraint fk_parcel_parking_lot foreign key(parcel_id) references land_use.parcel(id)
);
select AddGeometryColumn ('land_use', 'parking_lot', 'geom', 3435, 'POLYGON', 2);
create index sidx_parking_lot_geom
    on land_use.parking_lot using gist
    (geom)
    tablespace pg_default;
