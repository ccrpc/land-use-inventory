create table land_use.lbcs_activity (
  id serial primary key,
  code varchar(4),
  description varchar(500)
);

create table land_use.lbcs_site (
  id serial primary key,
  code varchar(4),
  description varchar(500)
);

create table land_use.lbcs_structure (
  id serial primary key,
  code varchar(4),
  description varchar(500)
);