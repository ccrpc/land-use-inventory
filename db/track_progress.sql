create view land_use.data_collection_progress as
select tp.total as points_completed,
	tpl.total as points_total,
	round(100.0 * tp.total / tpl.total, 1) as percent_complete
from (
	select count(*) as total
	from land_use.target_photo
	group by true
) as tp
inner join (
	select count(*) as total
	from land_use.target_photo_location
	group by true
) as tpl
on true;