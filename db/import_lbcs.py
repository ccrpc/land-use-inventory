import csv
import psycopg2
from config import DB

conn = psycopg2.connect(
    host=DB['host'],
    port=DB['port'],
    user=DB['user'],
    password=DB['password'],
    database=DB['dbname'],
)


def process_row(row: list) -> dict:
    code_number = row.pop(0)
    filtered_row = [value for value in row if value != ""]
    description = " ".join(filtered_row)
    return [code_number, description]


def process_lbcs(file_name: str):
    code = {}
    with open(file_name) as csvfile:
        lbcs_reader = csv.reader(csvfile)
        for row in lbcs_reader:
            code_number, description = process_row(row)
            code[code_number] = description
    return code


def import_to_db(file: str, code_dict: dict):
    table = file.split('.')[0]
    cursor = conn.cursor()
    for code, description in code_dict.items():
        print(code, description)
        cursor.execute(f"""
        INSERT INTO land_use.lbcs_{table} (code, description)
        VALUES (%s, %s);
        """, (code, description))
        conn.commit()
    cursor.close()


def main(file_array: list):
    for file in file_array:
        code_dict = process_lbcs(file)
        import_to_db(file, code_dict)


if __name__ == "__main__":
    main(["activity.csv", "site.csv", "structure.csv"])
