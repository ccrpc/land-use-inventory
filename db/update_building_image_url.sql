WITH view_data AS (
    SELECT
        b.building_id AS b_id,
        tpl.id AS tpl_id,
        f.timestamp AS ts,
        f.accelerometer_x AS x,
        f.accelerometer_y AS y,
        f.accelerometer_z AS z,
        st_azimuth(f.geom, st_centroid(b.geom)) - f.vehicle_heading AS rotation,
        ROUND(atan(f.accelerometer_x / f.accelerometer_z)::NUMERIC, 4) AS pan,
        CASE 
            WHEN ABS(f.accelerometer_y) > ABS(f.accelerometer_z)
            THEN ROUND(atan(f.accelerometer_y / f.accelerometer_z)::NUMERIC, 4)
            ELSE 0.0 END AS tilt
    FROM land_use.building AS b
    JOIN LATERAL (
        SELECT
            id,
            geom
        FROM land_use.target_photo_location
        ORDER BY geom <-> b.geom
        LIMIT 1
    ) AS tpl ON st_dwithin(b.geom, tpl.geom, 500)
    JOIN LATERAL (
        SELECT * FROM land_use.frame AS f
        WHERE target_photo_location_id IS NOT NULL
            AND f.quality_ok = true 
            AND f.gps_valid = true
            AND f.accelerometer_x IS NOT NULL
            AND f.accelerometer_y IS NOT NULL
            AND f.accelerometer_z IS NOT NULL
            AND f.vehicle_heading IS NOT NULL
            AND ABS(atan(f.accelerometer_y / f.accelerometer_z)) < 0.9
        ORDER BY f.geom <-> tpl.geom
        LIMIT 1
    ) AS f ON st_dwithin(f.geom, tpl.geom, 500)
)
SELECT
    b_id,
    'http://lui.ccrpc.org/image/'
        || view_data.tpl_id || '_' || REGEXP_REPLACE(SUBSTRING(view_data.ts::TEXT FROM '#"____-__-__#"%' FOR '#'), '-', '', 'g')
        || '?rotation=' || COALESCE(rotation, 0)
        || '&pan='      || COALESCE(pan, 0)
        || '&tilt='     || COALESCE(tilt, 0) AS url
FROM view_data

---- NOTE: Disable the SELECT statement above and enable the UPDATE statement bellow to update the building table

-- UPDATE land_use.building
-- -- Use arctangent to calculate corrective angle on the xz and yz planes.
-- -- Since camera has some self-correction, only do this when angle is off by more than pi/2 radians.
-- SET image_url = 'http://lui.ccrpc.org/image/' 
--      || view_data.tpl_id || '_' || REGEXP_REPLACE(SUBSTRING(view_data.ts::TEXT FROM '#"____-__-__#"%' FOR '#'), '-', '', 'g') 
--      || '?rotation=' || COALESCE(rotation, 0)
--      || '&pan='      || COALESCE(pan, 0)
--      || '&tilt='     || COALESCE(tilt, 0)
-- FROM view_data
-- WHERE building.building_id = b_id
-- RETURNING    
--  building.building_id, 
--  building.image_url,
--  x,
--  y,
--  z
