import geopandas as gpd
from config import ENGINE, PARCEL_PATH
from sqlalchemy import create_engine


def read_geopackage(path, layer):
    return gpd.read_file(path, layer=layer)


def sanitize_parcel(gdf):
    gdf = gdf.rename(columns={"PIN": "parcel_id"})
    gdf = gdf[["parcel_id", "geometry"]]
    return gdf


def import_to_postgis(gdf):
    gdf.to_postgis(
        name="parcel",
        con=ENGINE,
        if_exists="replace",
        schema="land_use_inventory",
    )


def main():
    parcel = read_geopackage(PARCEL_PATH, "tax_parcel")
    sanitized_parcel = sanitize_parcel(parcel)
    import_to_postgis(sanitized_parcel)


if __name__ == "__main__":
    main()
