create view land_use.target_photo as
select target.id,
  target.segment_id,
  target.pt_index,
  photo.id as photo_id,
  photo.from_video as photo_from_video,
  photo.date_captured as photo_date_captured,
  target.geom
from land_use.target_photo_location as target
inner join land_use.photo as photo
  on target.id = photo.target_photo_location_id;
