create view land_use.route as
select row_number() over () as id,
	*
from (
	select frame.video, 
		min(frame.timestamp) as start_time,
		min(frame.timestamp) as end_time,
		ST_Simplify(ST_MakeLine(frame.geom ORDER BY frame.timestamp), 5) As geom
	from land_use.frame as frame
	where frame.gps_valid
	group by frame.video
) as routes;
