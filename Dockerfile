FROM node:15-alpine3.10

LABEL version="0.1"
LABEL description="Node image processing container for land-use-inventory"

WORKDIR /usr/land-use-inventory

COPY ./package.json ./yarn.lock /usr/land-use-inventory/

RUN apk add --no-cache git bash ffmpeg

COPY src /usr/land-use-inventory/src/

RUN yarn install

# TODO: Add entry point here when parent application is ready