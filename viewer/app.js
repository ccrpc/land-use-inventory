import express from 'express';
import getPresignedUrl from './presignedUrl.js';
   
var app = express();

// configure Express
app.configure(function () {
  // app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.logger());
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }))
  // app.use(express.methodOverride());
  app.use(app.router);
  app.use('/dist', express.static('dist'));
});

app.get('/image/:image_id', function (req, res) {
  // Parse url and render page with image information
  let imgUrl = getPresignedUrl(req.params.image_id)
  res.render('index', { imgUrl: imgUrl });
});

// TODO: Break this out.
app.listen(3000)
