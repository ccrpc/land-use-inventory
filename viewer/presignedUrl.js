import AWS from 'aws-sdk';
const credentials = {
    accessKeyId: process.env.S3_ACCESS_KEY,
    secretAccessKey: process.env.S3_SECRET_KEY
};
AWS.config.update({
    credentials: credentials,
    region: process.env.S3_REGION
});

const s3 = new AWS.S3();

export default (image_id) => s3.getSignedUrl('getObject', {
    Bucket: process.env.S3_BUCKET,
    Expires: 3600, // time to expire in seconds
    Key: `images/${image_id}.jpg`,
});
