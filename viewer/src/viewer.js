import { Viewer } from 'photo-sphere-viewer';
import CubemapAdapter from './cubemap';


window.displayImage = function (imageUrl) {
  const urlParams = new URLSearchParams(window.location.search);
  imageUrl = imageUrl.replaceAll('&amp;', '&');
  let panos = [
    {
      url : {
        left  : {
          url: imageUrl,
          size: 1344,
          left: 0,
          top: 0,
          rotation: 0
        },
        front : {
          url: imageUrl,
          size: 1344,
          left: 1344,
          top: 0,
          rotation: 0
        },
        right : {
          url: imageUrl,
          size: 1344,
          left: 2688,
          top: 0,
          rotation: 0
        },
        back  : {
          url: imageUrl,
          size: 1344,
          left: 1344,
          top: 1344,
          rotation: 1.5 * Math.PI
        },
        top   : {
          url: imageUrl,
          size: 1344,
          left: 2688,
          top: 1344,
          rotation: 1.5 * Math.PI
        },
        bottom: {
          url: imageUrl,
          size: 1344,
          left: 0,
          top: 1344,
          rotation: 1.5 * Math.PI
        }
      }
    }
  ];
  
  return new Viewer({
    container: 'viewer',
    adapter: CubemapAdapter,
    panorama: panos[0].url,
    navbar: [
      'autorotate', 'zoom', 'move'
    ],
    sphereCorrection: {
      tilt: parseFloat(urlParams.get('tilt') || '0'),
      pan: parseFloat(urlParams.get('rotation') || '0'),
      roll: parseFloat(urlParams.get('roll') || '0')
    },
    defaultZoomLvl: 0.01,
    // defaultLong: urlParams['rotation'] || 0.0
  });
}
