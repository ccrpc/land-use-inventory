const path = require('path');
const express = require('express');
const morgan = require('morgan');
const favicon = require('serve-favicon');
const serveStatic = require('serve-static');
const { createProxyMiddleware } = require('http-proxy-middleware');
const asyncHandler = require('express-async-handler');
const bearer = require('./scripts/features-token');
const { ensureAuthenticated, setupAuthentication } = require('./scripts/authentication');

// Used as body-parser (which is what express.json() uses below) turns the req.body into an object and the proxy needs to re-stream it to work.
function restream(proxyReq, req, res) {
  if (req.body) {
    const bodyData = JSON.stringify(req.body);
    proxyReq.setHeader('Content-Type', 'application/json');
    proxyReq.setHeader('Content-Length', Buffer.byteLength(bodyData));
    // stream the content
    proxyReq.write(bodyData);
  }
}

// Setting up the proxies which will be used to connect to different parts of the application
const featuresProxy = createProxyMiddleware({
  target: 'http://features:8080',
  logLevel: 'debug',
  pathRewrite: {
    '^/features': '',
  },
  onProxyReq: restream,
});
const trexProxy = createProxyMiddleware({
  target: 'http://trex:6767',
  logLevel: 'debug',
  pathRewrite: {
    '^/trex': '',
  },
  onProxyReq: restream,
});
const viewerProxy = createProxyMiddleware({
  target: 'http://viewer:3000',
  logLevel: 'debug',
  pathRewrite: {
    '^/viewer': '',
  },
  onProxyReq: restream,
});

// Add a bearer token to the Authorization header when communicate with the feature api.
function addBearer(req, res, next) {
  return asyncHandler(async (req, res, next) => {
    let token;
    try {
      token = await bearer();
    } catch (error) {
      console.log('Failed to get or create token');
      next(error);
    }
    req.headers['Authorization'] = 'Bearer ' + token;
    return next();
  })(req, res, next);
}

function insertLastEdit(req, res, next) {
  if (req.method === 'PATCH' || req.method === 'POST') {
    if (req.session?.passport?.user && req.body?.properties) {
      let email = '';
      if (req.session.passport.user.provider === 'microsoft') {
        console.log(req.session.passport.user.emails);
        email = req.session.passport.user.emails
          .map((email) => email.value.toLowerCase())
          .join(',');
      } else if (req.session.passport.user.provider === 'arcgis') {
        email = req.session.passport.user.email;
      }
      req.body.properties['last_edited'] = email;
    }
  }
  return next();
}

const app = express();
app.use(morgan('common'));
app.use(favicon(path.join(__dirname, 'www/assets/icon', 'favicon.ico')));
app.use(express.json());
setupAuthentication(app);

app.use('/features', ensureAuthenticated, insertLastEdit, addBearer, featuresProxy);
app.use(['/trex', '**/*.pbf'], ensureAuthenticated, trexProxy);
app.use(['/viewer', '/image', '/dist'], ensureAuthenticated, viewerProxy);
app.use(ensureAuthenticated, serveStatic('www'));

app.listen(3000);
