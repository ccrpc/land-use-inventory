const session = require('express-session');
const passport = require('passport');
const MicrosoftStrategy = require('passport-microsoft').Strategy;
const ArcGISStrategy = require('passport-arcgis').Strategy;
const cookieParser = require('cookie-parser');

// Simple route middleware to ensure user is authenticated.
//   Use this route middleware on any resource that needs to be protected.  If
//   the request is authenticated (typically via a persistent login session),
//   the request will proceed.  Otherwise, the user will be redirected to the
//   login page.
function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    res.cookie('display_name', req.session.passport.user.displayName);
    return next();
  } else {
    req.session.returnTo = req.originalUrl;
    res.redirect('/login');
  }
}

function AuthorizedEmailsIncludeEmail(authorizedEmails, userEmails) {
  return authorizedEmails
    .split(',')
    .map((m) => m.toLowerCase())
    .filter((authed) => userEmails.map((m) => m.value.toLowerCase()).includes(authed))
    .length;
}

// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing. However, since this example does not
//   have a database of user records, the complete Microsoft graph profile is
//   serialized and deserialized.
passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (obj, done) {
  done(null, obj);
});

// Use the MicrosoftStrategy within Passport.
//   Strategies in Passport require a `verify` function, which accept
//   credentials (in this case, an accessToken, refreshToken, and 37signals
//   profile), and invoke a callback with a user object.
passport.use(
  new MicrosoftStrategy(
    {
      clientID: process.env.MICROSOFT_GRAPH_CLIENT_ID,
      clientSecret: process.env.MICROSOFT_GRAPH_CLIENT_SECRET,
      callbackURL: `${process.env.APP_URL}/auth/microsoft/callback`,
      scope: ['user.read'],
    },
    function (accessToken, refreshToken, profile, done) {
      // asynchronous verification, for effect...
      process.nextTick(function () {
        // Check here to make sure user is in authorized email list
        if (
          AuthorizedEmailsIncludeEmail(
            process.env.MICROSOFT_AUTHORIZED_EMAILS,
            profile.emails
          )
        ) {
          return done(null, profile);
        } else {
          return done(null, null, {
            name: profile.displayName,
            emails: profile.emails.map((email) => ({
              type: email.type,
              email: email.value.toLowerCase(),
            })),
          });
        }
      });
    }
  )
);

passport.use(
  new ArcGISStrategy(
    {
      clientID: process.env.ARCGIS_CLIENT_ID,
      clientSecret: process.env.ARCGIS_CLIENT_SECRET,
      callbackURL: `${process.env.APP_URL}/auth/arcgis/callback`,
      scope: ['user.read'],
    },
    function (accessToken, refreshToken, profile, done) {
      process.nextTick(function () {
        if (
          AuthorizedEmailsIncludeEmail(process.env.ARCGIS_AUTHORIZED_EMAILS, [
            { value: profile.email },
          ])
        ) {
          return done(null, profile);
        } else {
          return done(null, null, {
            name: profile.displayName,
            emails: [
              {
                type: 'ArcGIS',
                email: profile.email.toLowerCase(),
              },
            ],
          });
        }
      });
    }
  )
);

function setup(app) {
  app.use(
    session({
      secret: process.env.SESSION_SECRET,
      resave: true,
      saveUninitialized: true,
    })
  );
  // Initialize Passport!  Also use passport.session() middleware, to support
  // persistent login sessions (recommended).
  app.use(cookieParser());
  app.use(passport.initialize());
  app.use(passport.session());

  app.set('view engine', 'ejs');

  app.get('/invalidUser', function (req, res) {
    if (req.isAuthenticated()) {
      res.redirect('/');
    }
    if (req.session && req.session.invalidProfile) {
      res.status(req.session.invalidProfile.error ? 500 : 200);
      res.render('invalidUser', {
        user: req.session.invalidProfile.user,
        error: req.session.invalidProfile.error,
        errMessage: req.session.invalidProfile.errMessage,
        method: req.session.invalidProfile.method,
      });
    } else {
      res.status(500);
      res.render('invalidUser', {
        user: undefined,
        error: true,
        errMessage: 'No log in attempt detected',
        method: 'Unknown',
      });
    }
  });

  app.get('/login', function (req, res) {
    res.render('login');
  });

  // GET /auth/microsoft
  //   Use passport.authenticate() as route middleware to authenticate the
  //   request. The first step in Microsoft Graph authentication will involve
  //   redirecting the user to the common Microsoft login endpoint. After authorization, Microsoft
  //   will redirect the user back to this application at /auth/microsoft/callback
  app.get('/auth/microsoft', passport.authenticate('microsoft'), function (req, res) {
    // The request will be redirected to Microsoft for authentication, so this
    // function will not be called.
  });

  app.get('/auth/arcgis', passport.authenticate('arcgis'), function (req, res) {
    // The request will be redirected to ArcGIS for authentication, so this
    // function will not be called.
  });

  // GET /auth/microsoft/callback
  //   Use passport.authenticate() as route middleware to authenticate the
  //   request.  If authentication fails, the user will be redirected back to the
  //   login page.  Otherwise, the primary route function function will be called,
  //   which, in this example, will redirect the user to the home page.

  // this is the redirect URL
  app.get('/auth/microsoft/callback', function (req, res, next) {
    passport.authenticate('microsoft', function (error, profile, invalidProfile) {
      if (error) {
        req.session.invalidProfile = {
          user: undefined,
          error: true,
          errMessage: error,
          method: 'Microsoft',
        };
        if (error.name === 'AuthorizationError' && error.code === 'consent_required') {
          console.log('Handling Microsoft user did not consent error');
          console.log(Object.entries(error));
          req.session.invalidProfile.errMessage = error.message.split('\r')[0];
        }
        return res.redirect('/invalidUser');
      }

      if (!profile && invalidProfile) {
        req.session.invalidProfile = {
          user: invalidProfile,
          error: false,
          method: 'Microsoft',
        };
        return res.redirect('/invalidUser');
      } else if (!profile) {
        return res.redirect('/login');
      }

      if (req.session.invalidProfile) {
        delete req.session.invalidProfile;
      }
      req.logIn(profile, function (err) {
        if (err) {
          return next(err);
        }
        return res.redirect(req.session.returnTo || '/');
      });
    })(req, res, next);
  });

  app.get('/auth/arcgis/callback', function (req, res, next) {
    passport.authenticate('arcgis', function (error, profile, invalidProfile) {
      if (error) {
        req.session.invalidProfile = {
          user: undefined,
          error: true,
          errMessage: error,
          method: 'ArcGIS',
        };
        return res.redirect('/invalidUser');
      }

      if (!profile && invalidProfile) {
        if (invalidProfile.message) {
          req.session.invalidProfile = {
            user: undefined,
            error: true,
            errMessage: invalidProfile.message,
            method: 'ArcGIS',
          };
        } else {
          req.session.invalidProfile = {
            user: invalidProfile,
            error: false,
            errMessage: '',
            method: 'ArcGIS',
          };
        }
        return res.redirect('/invalidUser');
      } else if (!profile) {
        return res.redirect('/login');
      }

      if (req.session.invalidUser) {
        delete req.session.invalidUser;
      }
      req.logIn(profile, function (err) {
        if (err) {
          return next(err);
        }
        return res.redirect(req.session.returnTo || '/');
      });
    })(req, res, next);
  });

  app.get('/logout', function (req, res) {
    req.logout();
    if (req.session.invalidProfile) {
      delete req.session.invalidProfile;
    }
    res.clearCookie('display_name');
    res.redirect('/');
  });
}

exports.ensureAuthenticated = ensureAuthenticated;
exports.setupAuthentication = setup;
