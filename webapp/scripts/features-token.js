const fs = require('fs');
const jwt = require('jsonwebtoken');
const { promisify } = require('util');

const configFilename = '/etc/webapp/features-config.json';
const configUser = 'lui_user';
let currentToken = undefined;
let currentAuth = undefined;

async function getAuth(filename, user) {
  let configFile = await promisify(fs.readFile)(filename);
  return JSON.parse(configFile).users[user]['auth'];
}

function generateToken(user, duration, auth) {
  let claims = {
    sub: user,
    exp: Math.ceil(new Date().getTime() / 1000) + duration,
  };
  if (auth.issuer) claims.iss = auth.issuer;
  if (auth.audience) claims.aud = auth.audience;

  const options = {
    algorithm: 'HS256',
  };

  return new Promise((resolve, reject) => {
    jwt.sign(claims, auth.secret, options, (err, token) => {
      err ? reject(err) : resolve(token);
    });
  });
}

async function verifyToken(user, token, auth) {
  let decoded;
  try {
    decoded = jwt.verify(token, auth.secret, {
      algorithms: ['HS256'],
      issuer: auth.issuer,
    });
  } catch (error) {
    console.log('Invalid token: ' + error.message);
    return false;
  }
  if (decoded.sub !== user) {
    console.log(`Token subject ${decoded.sub} does not match user ${user}`);
    return false;
  } else {
    // console.log('User: ' + decoded.sub);
    // console.log('Issuer: ' + decoded.iss);
    // console.log('Issued: ' + new Date(decoded.iat * 1000).toLocaleString());
    // console.log('Expires: ' + new Date(decoded.exp * 1000).toLocaleString());
    return true;
  }
}

async function isValid(user, token, auth) {
  if (token) {
    const valid = await verifyToken(user, token, auth);
    if (valid) {
      return true;
    }
  }
  return false;
}

async function generateBearer() {
  if (!currentAuth) {
    currentAuth = await getAuth(configFilename, configUser);
  }
  const valid = await isValid(configUser, currentToken, currentAuth);
  if (!valid) {
    currentToken = await generateToken(configUser, 17280, currentAuth);
  }
  return currentToken;
}

module.exports = generateBearer;
