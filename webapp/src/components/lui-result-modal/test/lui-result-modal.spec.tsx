import { newSpecPage } from '@stencil/core/testing';
import { LuiResultModal } from '../lui-result-modal';

describe('lui-result-modal', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [LuiResultModal],
      html: `<lui-result-modal></lui-result-modal>`,
    });
    expect(page.root).toEqualHtml(`
      <lui-result-modal>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </lui-result-modal>
    `);
  });
});
