import { newE2EPage } from '@stencil/core/testing';

describe('lui-result-modal', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<lui-result-modal></lui-result-modal>');

    const element = await page.find('lui-result-modal');
    expect(element).toHaveClass('hydrated');
  });
});
