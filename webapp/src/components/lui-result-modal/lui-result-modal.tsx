import { Component, Host, h, Prop, Element } from '@stencil/core';
import {
  parcelInterface,
  buildingInterface,
  featureInterface,
  parkingLotInterface,
} from '../lui-style/config/featureInterfaces';
import { featureTitles, luiLog } from '../utils';

@Component({
  tag: 'lui-result-modal',
  styleUrl: 'lui-result-modal.css',
  shadow: true,
})
export class LuiResultModal {
  @Element() el: HTMLLuiResultModalElement;
  /** The feature to display (assuming the operation was successful) */
  @Prop() feature: featureInterface;
  /** The action taken to result in this results modal, i.e. 'edit' */
  @Prop() action: string;
  /** Whether an error occurred */
  @Prop() error: boolean;
  /** If an error occurred, its message */
  @Prop() errorText: string;

  render() {
    let id: number = 0;
    let title: string = 'Unknown';
    if (!this.feature) {
      title = 'None';
    } else {
      id = this.feature.id;
      title = featureTitles[this.feature.featureType];
    }

    if (this.error) {
      return (
        <Host>
          <ion-header>
            <ion-toolbar>
              <ion-title>
                Error during {this.action.charAt(0).toUpperCase() + this.action.slice(1)}
              </ion-title>
              <ion-buttons slot="end">
                <ion-button fill="outline" onClick={() => this.closeModal()}>
                  Close
                  <ion-icon name="close"></ion-icon>
                </ion-button>
              </ion-buttons>
            </ion-toolbar>
          </ion-header>
          <ion-content>
            <ion-grid>
              <ion-row>
                <ion-col class="error">{this.errorText}</ion-col>
              </ion-row>
            </ion-grid>
          </ion-content>
        </Host>
      );
    } else {
      return (
        <Host>
          <ion-header>
            <ion-toolbar color="secondary">
              <ion-title>
                Success {this.action.charAt(0).toUpperCase() + this.action.slice(1)}{' '}
                {title} {id}
              </ion-title>
              <ion-buttons slot="end">
                <ion-button fill="outline" onClick={() => this.closeModal()}>
                  {this.action === 'create' ? 'Continue' : 'Close'}
                  <ion-icon name="close"></ion-icon>
                </ion-button>
              </ion-buttons>
            </ion-toolbar>
          </ion-header>
          <ion-content>{this.action !== 'create' ? this.featureGrid() : ''}</ion-content>
        </Host>
      );
    }
  }

  private featureGrid() {
    if (!this.feature) {
      return <ion-label class="header">No Feature Selected</ion-label>;
    }
    switch (this.feature.featureType) {
      case 'parcel':
        return this.resultParcelGrid(this.feature as parcelInterface);
      case 'building':
        return this.resultBuildingGrid(this.feature as buildingInterface);
      case 'parkingLot':
        return this.resultParkingLotGrid(this.feature as parcelInterface);
      default:
        luiLog(
          'info',
          'attempting to display feature grid of feature without a resultModalGrid'
        );
        luiLog('info', this.feature);
        return (
          <ion-grid>
            <ion-row>
              <ion-col class="error">
                <ion-label class="header">
                  Feature of Unknown Type: {this.feature.featureType}
                </ion-label>
              </ion-col>
            </ion-row>
          </ion-grid>
        );
    }
  }

  private async closeModal(): Promise<void> {
    await (this.el.closest('ion-modal') as HTMLIonModalElement).dismiss();
  }

  private resultAttributeCol(
    title: string,
    attribute: any = undefined,
    alternative: string = 'Unknown',
    size: number = 6,
    link: boolean = false
  ) {
    let linkTarget: string = attribute;
    if (typeof attribute === 'string' || attribute instanceof String) {
      attribute = attribute.replace(/,(?=[^\s])/g, ', ');
      linkTarget = linkTarget.replace(/http:\/\/lui.ccrpc.org/g, '');
    }
    const linked = (
      <a href={linkTarget} target="_blank" rel="noopener noreferrer">
        {attribute}
      </a>
    );
    return (
      <ion-col size={size.toString()} class="attributes">
        <p>{title}</p>
        {attribute !== undefined && attribute !== null
          ? link
            ? linked
            : attribute
          : alternative}
      </ion-col>
    );
  }

  private resultParcelGrid(parcel: parcelInterface) {
    if (!parcel) {
      return <p style={{ 'text-align': 'center' }}>No Parcel Selected</p>;
    }
    return (
      <ion-grid>
        <ion-row>
          {this.resultAttributeCol('Viewer Preview', parcel.image_url, 'None', 12, true)}
        </ion-row>
        <ion-row>
          {this.resultAttributeCol('LBCS Site', parcel.lbcs_site)}
          {this.resultAttributeCol('LBCS Activity', parcel.lbcs_activity)}
        </ion-row>
        <ion-row>
          {this.resultAttributeCol('APROP Land Use Code', parcel.aprop)}
          {this.resultAttributeCol('Buildings', parcel.buildings)}
        </ion-row>
        <ion-row>
          {this.resultAttributeCol(
            'Impervious Area (in sq feet)',
            parcel.impervious_area
          )}
          {this.resultAttributeCol('Parcel Pin', parcel.pin)}
        </ion-row>
        <ion-row>
          {this.resultAttributeCol('Date Last Edited', parcel.date)}
          {this.resultAttributeCol('Last Edited By', parcel.last_edited)}
        </ion-row>
        <ion-row>
          {this.resultAttributeCol('Comment', parcel.external_comment, 'None', 12)}
        </ion-row>
      </ion-grid>
    );
  }

  private resultBuildingGrid(building: buildingInterface) {
    if (!building) {
      return <p style={{ 'text-align': 'center' }}>No Building Selected</p>;
    }
    return (
      <ion-grid>
        <ion-row>
          {this.resultAttributeCol(
            'Viewer Preview',
            building.image_url,
            'None',
            12,
            true
          )}
        </ion-row>
        <ion-row>
          {this.resultAttributeCol('Street Name', building.street_name)}
          {this.resultAttributeCol('LBCS Structure', building.lbcs_structure)}
        </ion-row>
        <ion-row>
          {this.resultAttributeCol('Stories', building.stories)}
          {this.resultAttributeCol('Units', building.units)}
        </ion-row>
        <ion-row>
          {this.resultAttributeCol(
            "Containing Parcel's Pin",
            building.parcel_pin,
            'None',
            12
          )}
        </ion-row>
        <ion-row>
          {this.resultAttributeCol(
            'Left Setback',
            building.left_setback,
            'Not Calculated'
          )}
          {this.resultAttributeCol(
            'Right Setback',
            building.right_setback,
            'Not Calculated'
          )}
        </ion-row>
        <ion-row>
          {this.resultAttributeCol(
            'Front Setback',
            building.front_setback,
            'Not Calculated'
          )}
          {this.resultAttributeCol(
            'Rear Setback',
            building.rear_setback,
            'Not Calculated'
          )}
        </ion-row>
        <ion-row>
          {this.resultAttributeCol('Date Last Edited', building.date)}
          {this.resultAttributeCol('Last Edited By', building.last_edited)}
        </ion-row>
        <ion-row>
          {this.resultAttributeCol('Comment', building.external_comment, 'None', 12)}
        </ion-row>
      </ion-grid>
    );
  }

  private resultParkingLotGrid(parkingLot: parkingLotInterface) {
    if (!parkingLot) {
      return <p style={{ 'text-align': 'center' }}>No Parking Lot Selected</p>;
    }

    let estimate: string = undefined;
    if (parkingLot.estimate !== undefined && parkingLot.estimate !== null) {
      if (parkingLot.estimate) {
        estimate = 'Yes';
      } else {
        estimate = 'No';
      }
    }

    return (
      <ion-grid>
        <ion-row>
          {this.resultAttributeCol('Parking Spaces', parkingLot.parking_spaces)}
          {this.resultAttributeCol('Is this an estimate?', estimate)}
        </ion-row>
        <ion-row>
          {this.resultAttributeCol('Area (in sq feet)', parkingLot.area_sq_ft)}
          {this.resultAttributeCol('Date Last Edited', parkingLot.date)}
        </ion-row>
        <ion-row>
          {this.resultAttributeCol(
            'Last Edited By',
            parkingLot.last_edited,
            'Unknown',
            12
          )}
        </ion-row>
        <ion-row>
          {this.resultAttributeCol('Comment', parkingLot.external_comment, 'None', 12)}
        </ion-row>
      </ion-grid>
    );
  }
}
