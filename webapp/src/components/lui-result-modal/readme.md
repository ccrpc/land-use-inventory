# lui-result-modal



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute    | Description                                                    | Type               | Default     |
| ----------- | ------------ | -------------------------------------------------------------- | ------------------ | ----------- |
| `action`    | `action`     | The action taken to result in this results modal, i.e. 'edit'  | `string`           | `undefined` |
| `error`     | `error`      | Whether an error occurred                                      | `boolean`          | `undefined` |
| `errorText` | `error-text` | If an error occurred, its message                              | `string`           | `undefined` |
| `feature`   | --           | The feature to display (assuming the operation was successful) | `featureInterface` | `undefined` |


## Dependencies

### Depends on

- ion-header
- ion-toolbar
- ion-title
- ion-buttons
- ion-button
- ion-icon
- ion-content
- ion-grid
- ion-row
- ion-col
- ion-label

### Graph
```mermaid
graph TD;
  lui-result-modal --> ion-header
  lui-result-modal --> ion-toolbar
  lui-result-modal --> ion-title
  lui-result-modal --> ion-buttons
  lui-result-modal --> ion-button
  lui-result-modal --> ion-icon
  lui-result-modal --> ion-content
  lui-result-modal --> ion-grid
  lui-result-modal --> ion-row
  lui-result-modal --> ion-col
  lui-result-modal --> ion-label
  ion-button --> ion-ripple-effect
  style lui-result-modal fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
