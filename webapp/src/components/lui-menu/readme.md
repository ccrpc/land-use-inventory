# lui-menu
The menu at the left side of the interactive map which displays information on the currently selected
features as well as being able to request they be edited.


<!-- Auto Generated Below -->


## Properties

| Property     | Attribute | Description                                                         | Type                  | Default     |
| ------------ | --------- | ------------------------------------------------------------------- | --------------------- | ----------- |
| `building`   | --        | Information on the currently selected building (if there is one)    | `buildingInterface`   | `undefined` |
| `parcel`     | --        | Information on the currently selected parcel (if there is one)      | `parcelInterface`     | `undefined` |
| `parkingLot` | --        | Information on the currently selected parking lot (if there is one) | `parkingLotInterface` | `undefined` |


## Events

| Event                | Description                                                    | Type                            |
| -------------------- | -------------------------------------------------------------- | ------------------------------- |
| `requestFeatureEdit` | The event that is emitted when the user asks to edit a feature | `CustomEvent<featureInterface>` |


## Dependencies

### Used by

 - [lui-app](../lui-app)

### Depends on

- ion-grid
- ion-row
- ion-col
- ion-button
- ion-content

### Graph
```mermaid
graph TD;
  lui-menu --> ion-grid
  lui-menu --> ion-row
  lui-menu --> ion-col
  lui-menu --> ion-button
  lui-menu --> ion-content
  ion-button --> ion-ripple-effect
  lui-app --> lui-menu
  style lui-menu fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
