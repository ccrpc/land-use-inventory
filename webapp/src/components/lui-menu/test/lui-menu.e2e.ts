import { newE2EPage } from '@stencil/core/testing';

describe('lui-menu', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<lui-menu></lui-menu>');

    const element = await page.find('lui-menu');
    expect(element).toHaveClass('hydrated');
  });
});
