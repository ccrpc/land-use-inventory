import { newSpecPage } from '@stencil/core/testing';
import { LuiMenu } from '../lui-menu';

describe('lui-menu', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [LuiMenu],
      html: `<lui-menu></lui-menu>`,
    });
    expect(page.root).toEqualHtml(`
      <lui-menu>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </lui-menu>
    `);
  });
});
