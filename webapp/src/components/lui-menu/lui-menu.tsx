import { Component, Host, h, Prop, State, Event, EventEmitter } from '@stencil/core';
import {
  buildingInterface,
  featureInterface,
  parcelInterface,
  parkingLotInterface,
} from '../lui-style/config/featureInterfaces';
import { featureTitles, roundNumber } from '../utils';

@Component({
  tag: 'lui-menu',
  styleUrl: 'lui-menu.css',
  shadow: true,
})
export class LuiMenu {
  /** Information on the currently selected building (if there is one) */
  @Prop() building: buildingInterface = undefined;
  /** Information on the currently selected parcel (if there is one) */
  @Prop() parcel: parcelInterface = undefined;
  /** Information on the currently selected parking lot (if there is one) */
  @Prop() parkingLot: parkingLotInterface = undefined;
  /** What feature to focus */
  @State() focus: string = 'parcel';
  /** The event that is emitted when the user asks to edit a feature */
  @Event() requestFeatureEdit: EventEmitter<featureInterface>;

  private changeFocus(focus: string) {
    if (this.focus !== focus) {
      this.focus = focus;
    }
  }

  render() {
    return (
      <Host>
        <ion-grid>
          <ion-row>
            <ion-col>
              <ion-button
                onClick={() => this.changeFocus('parcel')}
                disabled={this.focus === 'parcel'}
                expand="full"
              >
                Focus Parcel
              </ion-button>
            </ion-col>
            <ion-col>
              <ion-button
                onClick={() => this.changeFocus('building')}
                disabled={this.focus === 'building'}
                expand="full"
              >
                Focus Building
              </ion-button>
            </ion-col>
            <ion-col>
              <ion-button
                onClick={() => this.changeFocus('parkingLot')}
                disabled={this.focus === 'parkingLot'}
                expand="full"
              >
                Focus Parking Lot
              </ion-button>
            </ion-col>
          </ion-row>
        </ion-grid>
        <p class="header">{featureTitles[this.focus]} Information</p>
        <ion-content>
          {this.featureGrid()}
          {this.editFeatureButton()}
        </ion-content>
      </Host>
    );
  }

  private featureGrid() {
    switch (this.focus) {
      case 'parcel':
        return this.menuParcelGrid(this.parcel);
      case 'building':
        return this.menuBuildingGrid(this.building);
      case 'parkingLot':
        return this.menuParkingLotGrid(this.parkingLot);
    }
  }

  private editFeatureButton() {
    if (
      (this.focus === 'parcel' && !this.parcel) ||
      (this.focus === 'building' && !this.building) ||
      (this.focus === 'parkingLot' && !this.parkingLot)
    ) {
      return;
    }
    return (
      <ion-grid>
        <ion-row>
          <ion-col style={{ 'text-align': 'center' }}>
            <ion-button
              fill="outline"
              onClick={() => {
                switch (this.focus) {
                  case 'parcel':
                    this.requestFeatureEdit.emit(this.parcel);
                    break;
                  case 'building':
                    this.requestFeatureEdit.emit(this.building);
                    break;
                  case 'parkingLot':
                    this.requestFeatureEdit.emit(this.parkingLot);
                    break;
                }
              }}
            >
              Edit {featureTitles[this.focus]}
            </ion-button>
          </ion-col>
        </ion-row>
      </ion-grid>
    );
  }

  private menuAttributeCol(
    title: string,
    attribute: any = undefined,
    alternative: string = 'Unknown',
    size: number = 6,
    link: boolean = false,
    titleIsLink: boolean = false,
    titleLinkTarget: string = ''
  ) {
    if (typeof attribute === 'boolean') {
      if (attribute) {
        attribute = 'True';
      } else {
        attribute = 'False';
      }
    }

    let linkTarget: string = attribute;
    if (attribute && (typeof attribute === 'string' || attribute instanceof String)) {
      attribute = attribute.replace(/,(?=[^\s])/g, ', ');
      linkTarget = linkTarget.replace(/http:\/\/lui.ccrpc.org/g, '');
    }
    const linked = (
      <a href={linkTarget} target="_blank" rel="noopener noreferrer">
        {attribute}
      </a>
    );

    const renderTitle = titleIsLink ? (
      <p>
        <a href={titleLinkTarget} target="_blank" rel="noopener noreferrer">
          {title}
        </a>
      </p>
    ) : (
      <p>{title}</p>
    );

    return (
      <ion-col size={size.toString()} class="attributes">
        {renderTitle}
        {attribute !== undefined && attribute !== null
          ? link
            ? linked
            : attribute
          : alternative}
      </ion-col>
    );
  }

  private menuParcelGrid(parcel: parcelInterface) {
    if (!parcel) {
      return <p style={{ 'text-align': 'center' }}>No Parcel Selected</p>;
    }
    return (
      <ion-grid>
        <ion-row>
          {this.menuAttributeCol('Parcel ID', parcel.id)}
          {this.menuAttributeCol('Viewer Preview', parcel.image_url, 'None', 6, true)}
        </ion-row>
        <ion-row>
          {this.menuAttributeCol(
            'LBCS Site',
            parcel.lbcs_site,
            'Unknown',
            6,
            false,
            true,
            'https://www.planning.org/lbcs/standards/site/'
          )}
          {this.menuAttributeCol(
            'LBCS Activity',
            parcel.lbcs_activity,
            'Unknown',
            6,
            false,
            true,
            'https://www.planning.org/lbcs/standards/activity/'
          )}
        </ion-row>
        <ion-row>
          {this.menuAttributeCol('APROP Land Use Code', parcel.aprop)}
          {this.menuAttributeCol('Buildings', parcel.buildings)}
        </ion-row>
        <ion-row>
          {this.menuAttributeCol(
            'Impervious Area (in sq feet)',
            roundNumber(parcel.impervious_area ? Number(parcel.impervious_area) : null, 1)
          )}
          {this.menuAttributeCol('Parcel Pin', parcel.pin)}
        </ion-row>
        <ion-row>
          {this.menuAttributeCol('Date Last Edited', parcel.date)}
          {this.menuAttributeCol('Last Edited By', parcel.last_edited)}
        </ion-row>
        <ion-row>
          {this.menuAttributeCol('Comment', parcel.external_comment, 'None', 12)}
        </ion-row>
      </ion-grid>
    );
  }

  private menuBuildingGrid(building: buildingInterface) {
    if (!building) {
      return <p style={{ 'text-align': 'center' }}>No Building Selected</p>;
    }
    return (
      <ion-grid>
        <ion-row>
          {this.menuAttributeCol('Building ID', building.id)}
          {this.menuAttributeCol('Viewer Preview', building.image_url, 'None', 6, true)}
        </ion-row>
        <ion-row>
          {this.menuAttributeCol('Street Name', building.street_name)}
          {this.menuAttributeCol(
            'LBCS Structure',
            building.lbcs_structure,
            'Unknown',
            6,
            false,
            true,
            'https://www.planning.org/lbcs/standards/structure/'
          )}
        </ion-row>
        <ion-row>
          {this.menuAttributeCol('Stories', building.stories)}
          {this.menuAttributeCol('Units', building.units)}
        </ion-row>
        <ion-row>
          {this.menuAttributeCol(
            "Containing Parcel's Pin",
            building.parcel_pin,
            'None',
            12
          )}
        </ion-row>
        <ion-row>
          {this.menuAttributeCol('Left Setback', building.left_setback, 'Not Calculated')}
          {this.menuAttributeCol(
            'Right Setback',
            building.right_setback,
            'Not Calculated'
          )}
        </ion-row>
        <ion-row>
          {this.menuAttributeCol(
            'Front Setback',
            building.front_setback,
            'Not Calculated'
          )}
          {this.menuAttributeCol('Rear Setback', building.rear_setback, 'Not Calculated')}
        </ion-row>
        <ion-row>
          {this.menuAttributeCol('Date Last Edited', building.date)}
          {this.menuAttributeCol('Last Edited By', building.last_edited)}
        </ion-row>
        <ion-row>
          {this.menuAttributeCol('Comment', building.external_comment, 'None', 12)}
        </ion-row>
      </ion-grid>
    );
  }

  private menuParkingLotGrid(parkingLot: parkingLotInterface) {
    if (!parkingLot) {
      return <p style={{ 'text-align': 'center' }}>No Parking Lot Selected</p>;
    }

    let estimate: string = undefined;

    if (parkingLot.estimate) {
      estimate = 'Yes';
    } else {
      estimate = 'No';
    }

    return (
      <ion-grid>
        <ion-row>
          {this.menuAttributeCol('Parking Lot ID', parkingLot.id, 'Unknown', 12)}
        </ion-row>
        <ion-row>
          {this.menuAttributeCol('Parking Spaces', parkingLot.parking_spaces)}
          {this.menuAttributeCol('Is this an estimate?', estimate)}
        </ion-row>
        <ion-row>
          {this.menuAttributeCol(
            'Area (in sq feet)',
            parkingLot.area_sq_ft,
            'Unknown',
            12
          )}
        </ion-row>
        <ion-row>
          {this.menuAttributeCol('Date Last Edited', parkingLot.date)}
          {this.menuAttributeCol('Last Edited By', parkingLot.last_edited)}
        </ion-row>
        <ion-row>
          {this.menuAttributeCol('Comment', parkingLot.external_comment, 'None', 12)}
        </ion-row>
      </ion-grid>
    );
  }
}
