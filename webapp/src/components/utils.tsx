import {
  buildingInterface,
  parcelInterface,
  parkingLotInterface,
} from './lui-style/config/featureInterfaces';

export function getMap(id?: string): HTMLGlMapElement {
  return document.querySelector(id ? `gl-map#${id}` : 'gl-map');
}

export function isObjectEmpty(obj: Object): boolean {
  for (let prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      return false;
    }
  }
  return true;
}

export const featureTitles: { [key: string]: string } = {
  parcel: 'Parcel',
  building: 'Building',
  parkingLot: 'Parking Lot',
};

export const featureTypeToTables: { [key: string]: string } = {
  parcel: 'parcel',
  building: 'building',
  parkingLot: 'parking_lot',
};

/**
 * Log an event or text.
 * @param logLevel What level of logging (currently 'info' or 'error')
 * @param item What to Log
 */
export function luiLog(logLevel: 'info' | 'error' | string, item: any): void {
  switch (logLevel) {
    case 'info':
      console.log(item);
      break;
    case 'error':
      alert(item);
      console.log(item);
      break;
    default:
      console.log(`Error of unknown logLevel - ${logLevel}: ${item} `);
      console.log(item);
  }
}

function nullEqual(first: any, second: any): boolean {
  if (
    (first === null && second === undefined) ||
    (first === undefined && second === null)
  ) {
    return true;
  }
  return first === second;
}

export function parcelEqual(first: parcelInterface, second: parcelInterface): boolean {
  if (first && second) {
    return (
      nullEqual(first.id, second.id) &&
      nullEqual(first.featureType, second.featureType) &&
      nullEqual(first.aprop, second.aprop) &&
      nullEqual(first.image_url, second.image_url) &&
      nullEqual(first.lbcs_activity, second.lbcs_activity) &&
      nullEqual(first.lbcs_site, second.lbcs_site) &&
      nullEqual(first.pin, second.pin) &&
      nullEqual(first.buildings, second.buildings) &&
      nullEqual(first.last_edited, second.last_edited)
    );
  }
  return false;
}

export function buildingEqual(
  first: buildingInterface,
  second: buildingInterface
): boolean {
  if (first && second) {
    return (
      nullEqual(first.id, second.id) &&
      nullEqual(first.featureType, second.featureType) &&
      nullEqual(first.parcel_pin, second.parcel_pin) &&
      nullEqual(first.image_url, second.image_url) &&
      nullEqual(first.lbcs_structure, second.lbcs_structure) &&
      nullEqual(first.front_setback, second.front_setback) &&
      nullEqual(first.left_setback, second.left_setback) &&
      nullEqual(first.rear_setback, second.rear_setback) &&
      nullEqual(first.right_setback, second.right_setback) &&
      nullEqual(first.stories, second.stories) &&
      nullEqual(first.street_name, second.street_name) &&
      nullEqual(first.units, second.units) &&
      nullEqual(first.date, second.date) &&
      nullEqual(first.last_edited, second.last_edited)
    );
  }
  return false;
}

export function parkingLotEqual(
  first: parkingLotInterface,
  second: parkingLotInterface
): boolean {
  if (first && second) {
    return (
      nullEqual(first.id, second.id) &&
      nullEqual(first.featureType, second.featureType) &&
      nullEqual(first.parking_spaces, second.parking_spaces) &&
      nullEqual(first.area_sq_ft, second.area_sq_ft) &&
      nullEqual(first.parcel_id, second.parcel_id) &&
      nullEqual(first.date, second.date) &&
      nullEqual(first.estimate, second.estimate) &&
      nullEqual(first.last_edited, second.last_edited)
    );
  }
  return false;
}

export function roundNumber(num: number, scale: number) {
  if (!String(num).includes('e')) {
    return Number(Math.round(Number(num + 'e+' + scale)) + 'e-' + scale);
  } else {
    const arr = String(num).split('e');
    let sig = '';
    if (Number(arr[1]) + scale > 0) {
      sig = '+';
    }
    return Number(
      Math.round(Number(arr[0] + 'e' + sig + (Number(arr[1]) + scale))) + 'e-' + scale
    );
  }
}
