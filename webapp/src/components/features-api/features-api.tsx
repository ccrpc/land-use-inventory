import { featureInterface } from '../lui-style/config/featureInterfaces';
import { featureTypeToTables, luiLog } from '../utils';

export interface FeatureAPIResult {
  response: Response;
  JSONdata: any;
  feature: featureInterface;
}

export class FeaturesApi {
  private dbTable: string;
  private dbUser: string;

  constructor(dbTable: string, dbUser: string) {
    this.dbTable = dbTable;
    this.dbUser = dbUser;
  }

  /** Takes the json data from the response from the features api and creates a feature from it */
  private convertToInterface(data: any, type: string): featureInterface {
    // Change arrays to strings of \{[value](, value)*\} string to match the interface
    for (let property in data.properties) {
      if ((data.properties as Object).hasOwnProperty(property)) {
        if (Array.isArray(data.properties[property])) {
          const propertyArray: Array<any> = data.properties[property];
          let str: string = '{';
          for (let [index, value] of propertyArray.entries()) {
            if (index !== 0) {
              str += ',';
            }
            str += value;
          }
          str += '}';
          data.properties[property] = str;
        }
      }
    }
    let result: featureInterface = {
      ...data.properties,
    };

    result.id = data.id;
    result.geometry = data.geometry;
    result.featureType = type;

    return result;
  }

  private convertToRequest(
    feature: featureInterface,
    deleteId: boolean = false
  ): BodyInit {
    let featureCopy: featureInterface = { ...feature };
    let requestBody = {};

    if (deleteId) {
      delete featureCopy['id'];
    }
    if (featureCopy.geometry) {
      requestBody['geometry'] = featureCopy.geometry;
      delete featureCopy['geometry'];
    }
    delete featureCopy['featureType'];
    requestBody['properties'] = featureCopy;

    return JSON.stringify(requestBody);
  }

  /** Get a feature via the GET Request */
  public async getFeature(id: number, type: string): Promise<FeatureAPIResult> {
    const url = `/features/${this.dbUser}/${this.dbTable}.${featureTypeToTables[type]}/${id}`;
    luiLog('info', `Attempting to fetch resource at ${url}`);

    const response: Response = await fetch(url, {
      method: 'GET',
    });
    const data = await response.json();
    let responseFeature: featureInterface = undefined;
    if (response.ok) {
      responseFeature = this.convertToInterface(data, type);
    }

    return {
      response: response,
      JSONdata: data,
      feature: responseFeature,
    };
  }

  /** Edit a feature via PATCH request */
  public async editFeature(
    id: number,
    type: string,
    feature: featureInterface
  ): Promise<FeatureAPIResult> {
    const url = `/features/${this.dbUser}/${this.dbTable}.${featureTypeToTables[type]}/${id}`;
    luiLog('info', `Attempting to edit resource at ${url}`);

    const response: Response = await fetch(url, {
      method: 'PATCH',
      headers: new Headers({
        'content-type': 'application/json',
      }),
      body: this.convertToRequest(feature, true),
    });
    const data = await response.json();
    let responseFeature: featureInterface = undefined;
    if (response.ok) {
      responseFeature = this.convertToInterface(data, type);
    }

    return {
      response: response,
      JSONdata: data,
      feature: responseFeature,
    };
  }

  /** Create a new feature via a POST request */
  public async pushFeature(
    type: string,
    feature: featureInterface
  ): Promise<FeatureAPIResult> {
    const url = `/features/${this.dbUser}/${this.dbTable}.${featureTypeToTables[type]}`;
    luiLog('info', `Attempting to push new resource at ${url}`);

    const response: Response = await fetch(url, {
      method: 'POST',
      headers: new Headers({
        'content-type': 'application/json',
      }),
      body: this.convertToRequest(feature, false),
    });
    const data = await response.json();

    let responseFeature: featureInterface = undefined;
    if (response.ok) {
      responseFeature = this.convertToInterface(data, type);
    }

    return {
      response: response,
      JSONdata: data,
      feature: responseFeature,
    };
  }
}
