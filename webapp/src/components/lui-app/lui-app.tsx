import { Component, h, Listen, Prop, State, Element } from '@stencil/core';
import {
  featureInterface,
  buildingInterface,
  parcelInterface,
  parkingLotInterface,
} from '../lui-style/config/featureInterfaces';
import { DrawOptions } from '@ccrpc/webmapgl/dist/types/components/draw-controller/interface';
import {
  isObjectEmpty,
  luiLog,
  parcelEqual,
  buildingEqual,
  parkingLotEqual,
} from '../utils';
import { modalController, OverlayEventDetail, menuController } from '@ionic/core';
import { FeaturesApi, FeatureAPIResult } from '../features-api/features-api';
import Cookies from 'js-cookie';
import mapboxgl from 'mapbox-gl';
import { uploadDetails } from '../lui-upload-modal/lui-upload-modal';

@Component({
  tag: 'lui-app',
  styleUrl: 'lui-app.css',
  shadow: false,
})
export class LuiApp {
  @Element() el: HTMLElement;

  /** Initial latitude center of the map */
  @Prop() lat: number = 40.11178;
  /** Initial longitude center of the map */
  @Prop() long: number = -88.20365;
  /** How zoomed in the application is */
  @Prop() zoom: number = 15;
  /** The title of the App */
  @Prop() label: string = 'Land Use Inventory';
  /** Information on the currently selected Parcel (if there is one) */
  @State() selectedParcel: parcelInterface = undefined;
  /** Information on the currently selected Building (if there is one) */
  @State() selectedBuilding: buildingInterface = undefined;
  /**Information on the currently selected Parking Lot (if there is one) */
  @State() selectedParkingLot: parkingLotInterface = undefined;
  /** Whether there is an active drawing session */
  @State() drawing: boolean = false;
  /** The display name as set by a cookie by the back-end
   * NOTE: use this for display purposes only. The user, albeit not trivially, is able to change this by changing the cookie.
   * It is an unsigned cookie as any attempts at signature checking in the stencil would have the
   * signing key available to the user if they search for it.
   */
  @State() private displayName: string;
  /** An instance of the Features api that connects to the REST API that allows interaction with the database */
  private featuresAPI: FeaturesApi;
  /** A cache to store changes the user has made to buildings while the trex cache catches up */
  private buildingCache: { [id: number]: buildingInterface };
  /** A cache to store changes the user has made to parcels while the trex cache catches up */
  private parcelCache: { [id: number]: parcelInterface };
  /** A cache to store changes the user has made to parking lots while the trex cache catches up */
  private parkingLotCache: { [id: number]: parkingLotInterface };
  /** A timer id used internally in the functionality to clear the feature caches */
  private cacheTimer: number = undefined;
  /** A reference to this object's gl-map element */
  private map: HTMLGlMapElement;

  /** If the user clicks on features, update the current building, parcel, and parking lot */
  @Listen('glClickedFeaturesGathered')
  updateSelectedFeatures(event: CustomEvent): void {
    this.selectedBuilding = undefined;
    this.selectedParcel = undefined;
    this.selectedParkingLot = undefined;
    for (const feature of event.detail.features) {
      this.updateDate(feature.properties as featureInterface);
      switch (feature.source) {
        case 'parcel:parcel':
          this.selectedParcel = feature.properties as parcelInterface;
          this.selectedParcel.featureType = 'parcel';
          break;
        case 'building:building':
          this.selectedBuilding = feature.properties as buildingInterface;
          this.selectedBuilding.id = this.selectedBuilding['building_id'];
          delete this.selectedBuilding['building_id'];
          this.selectedBuilding.featureType = 'building';
          break;
        case 'parking_lot:parking_lot':
          this.selectedParkingLot = feature.properties as parkingLotInterface;
          this.selectedParkingLot.featureType = 'parkingLot';
          break;
        default:
          luiLog('error', `Unknown Feature Type: ${feature.source}`);
          luiLog('info', 'Feature with unknown feature type');
          luiLog('info', feature);
          break;
      }
    }
    this.checkAllCache();
  }

  checkAllCache(): void {
    let clearCache: boolean = false;
    let inCache: boolean = false;
    if (this.selectedBuilding && this.buildingCache[this.selectedBuilding.id]) {
      luiLog(
        'info',
        `building with id ${this.selectedBuilding.id} found in building cache`
      );
      if (
        !buildingEqual(
          this.selectedBuilding,
          this.buildingCache[this.selectedBuilding.id]
        )
      ) {
        this.selectedBuilding = this.buildingCache[this.selectedBuilding.id];
        inCache = true;
      } else {
        clearCache = true;
      }
    }
    if (this.selectedParcel && this.parcelCache[this.selectedParcel.id]) {
      luiLog('info', `parcel with id ${this.selectedParcel.id} found in parcel cache`);
      if (!parcelEqual(this.selectedParcel, this.parcelCache[this.selectedParcel.id])) {
        this.selectedParcel = this.parcelCache[this.selectedParcel.id];
        inCache = true;
      } else {
        clearCache = true;
      }
    }
    if (this.selectedParkingLot && this.parkingLotCache[this.selectedParkingLot.id]) {
      luiLog(
        'info',
        `parking lot with id ${this.selectedParkingLot.id} found in parking lot cache`
      );
      if (
        !parkingLotEqual(
          this.selectedParkingLot,
          this.parkingLotCache[this.selectedParkingLot.id]
        )
      ) {
        this.selectedParkingLot = this.parkingLotCache[this.selectedParkingLot.id];
        inCache = true;
      } else {
        clearCache = true;
      }
    }
    if (clearCache) {
      luiLog('info', 'trex tile server up to date with cache, clearing cache');
      this.parcelCache = {};
      this.buildingCache = {};
      this.parkingLotCache = {};
    } else if (inCache && !this.cacheTimer) {
      luiLog('info', 'Setting cache clear timer');
      this.cacheTimer = window.setTimeout(() => {
        luiLog('info', 'Cache timer ran out, clearing cache');
        this.parcelCache = {};
        this.buildingCache = {};
        this.parkingLotCache = {};
        this.cacheTimer = undefined;
      }, 300000);
    }
  }

  /**
   * Updates the ['date'] field in the feature to be a human readable datestring.
   * @param feature The feature whose date to update
   */
  updateDate(feature: featureInterface): void {
    if (feature && feature.date) {
      const dateArray: number[] = feature.date.split('T')[0].split('-').map(Number);
      if (dateArray.length === 3) {
        // In javascript months start at 0 while postgis (which is what is setting these dates) starts at 1. Therefore we must subtract 1 from the month.
        feature.date = new Date(
          dateArray[0],
          dateArray[1] - 1,
          dateArray[2]
        ).toDateString();
      } else {
        luiLog('info', `Parsed date has incorrect number of arguments ${dateArray}`);
      }
    }
  }

  @Listen('glDrawEnter')
  drawHasStarted(): void {
    this.drawing = true;
  }

  @Listen('glDrawExit')
  drawHasEnded(): void {
    this.drawing = false;
  }

  @Listen('requestFeatureEdit')
  async onEdit(event: CustomEvent<featureInterface>): Promise<void> {
    this.openEditModal(event.detail);
  }

  /** Open a modal to edit the requested feature.
   * At the end of this a results modal with the Features API response to the request will be displayed.
   */
  async openEditModal(toEdit: featureInterface): Promise<void> {
    const modal: HTMLIonModalElement = await modalController.create({
      component: 'lui-edit-modal',
      backdropDismiss: false,
      componentProps: {
        feature: toEdit,
      },
    });

    modal.onDidDismiss().then(async (editDetails: OverlayEventDetail) => {
      if (editDetails.data && !isObjectEmpty(editDetails.data)) {
        let feature: featureInterface = editDetails.data.feature;
        const type: string = editDetails.data.type;
        await this.editFeature(feature, type);
      } else {
        luiLog('info', 'Modal was dismissed');
      }
    });

    await modal.present();
  }

  /** Starts a drawing session */
  async startDraw(type: string): Promise<void> {
    const drawController: HTMLGlDrawControllerElement =
      this.el.querySelector('gl-draw-controller');
    const drawOptions: DrawOptions = {
      mapId: 'mainMap',
      type: 'polygon',
      toolbarLabel: 'Draw New Building Geometry',
    };
    const createdFeatures = await drawController.create(null, drawOptions);
    // As 'multiple' is false (via default in webmapgl) we know we only have one feature to deal with and we
    // know we want it to be a building.
    // Note that if the user clicked "cancel" for the drawing drawController will return undefined.
    if (
      createdFeatures &&
      createdFeatures.features &&
      createdFeatures.features.length != 0
    ) {
      const newFeature: buildingInterface & parkingLotInterface = {
        geometry: createdFeatures.features[0].geometry,
        featureType: type,
        id: undefined,
      };
      if (type === 'building') {
        newFeature.study_area = true;
      }

      const createResult: FeatureAPIResult = await this.featuresAPI.pushFeature(
        type,
        newFeature
      );

      const resultModal: HTMLIonModalElement = await modalController.create({
        component: 'lui-result-modal',
        backdropDismiss: true,
        componentProps: {
          feature: createResult.feature,
          action: 'create',
          error: !createResult.response.ok,
          errorText: createResult.response.ok
            ? ''
            : 'Create Failed: ' + createResult.JSONdata.error,
        },
      });

      if (createResult.response.ok) {
        resultModal.onDidDismiss().then(async () => {
          const getResult: FeatureAPIResult = await this.featuresAPI.getFeature(
            createResult.feature.id,
            type
          );
          if (getResult.response.ok) {
            await this.openEditModal(getResult.feature);
          } else {
            const getResultModal: HTMLIonModalElement = await modalController.create({
              component: 'lui-result-modal',
              backdropDismiss: true,
              componentProps: {
                feature: getResult.feature,
                action: 'get',
                error: true,
                errorText: 'Get failed: ' + getResult.JSONdata.error,
              },
            });
            await getResultModal.present();
          }
        });
      }

      await resultModal.present();
    }
  }

  async editFeature(feature: featureInterface, type: string) {
    if (type !== 'building' && type !== 'parcel' && type !== 'parkingLot') {
      luiLog('error', `Feature with Unknown type: ${type}`);
      luiLog('info', 'Feature with unknown type');
      luiLog('info', feature);
      return;
    }
    let id: number = feature.id;
    const editResult: FeatureAPIResult = await this.featuresAPI.editFeature(
      id,
      type,
      feature
    );
    let resultFeature: featureInterface;
    let didError: boolean = true;
    let errorText: string;

    if (editResult.response.ok) {
      const getResult: FeatureAPIResult = await this.featuresAPI.getFeature(
        editResult.feature.id,
        type
      );
      if (getResult.response.ok) {
        didError = false;
        resultFeature = getResult.feature;
        this.updateDate(resultFeature);
        switch (resultFeature.featureType) {
          case 'building':
            this.buildingCache[resultFeature.id] = resultFeature as buildingInterface;
            luiLog(
              'info',
              `building with id ${resultFeature.id} stored in building cache`
            );
            break;
          case 'parcel':
            this.parcelCache[resultFeature.id] = resultFeature as parcelInterface;
            luiLog('info', `parcel with id ${resultFeature.id} stored in parcel cache`);
            break;
          case 'parkingLot':
            this.parkingLotCache[resultFeature.id] = resultFeature as parkingLotInterface;
            luiLog(
              'info',
              `parking lot with id ${resultFeature.id} stored in parking lot cache`
            );
        }
        this.checkAllCache();
      } else {
        errorText = 'Error in get request: ' + getResult.JSONdata.error;
      }
    } else {
      errorText = 'Error in edit request: ' + editResult.JSONdata.error;
    }

    const resultModal: HTMLIonModalElement = await modalController.create({
      component: 'lui-result-modal',
      backdropDismiss: true,
      componentProps: {
        feature: resultFeature,
        action: 'edit',
        error: didError,
        errorText: errorText,
      },
    });

    await resultModal.present();
  }

  async beginUpload() {
    const uploadModal: HTMLIonModalElement = await modalController.create({
      component: 'lui-upload-modal',
      backdropDismiss: false,
      componentProps: {
        selectedBuilding: this.selectedBuilding,
        selectedParkingLot: this.selectedParkingLot,
      },
    });

    uploadModal
      .onDidDismiss()
      .then(async (uploadDetails: OverlayEventDetail<uploadDetails>) => {
        if (uploadDetails.data && uploadDetails.data.success) {
          const feature: featureInterface = {
            geometry: uploadDetails.data.geometry,
            id: uploadDetails.data.id,
            featureType: uploadDetails.data.featureType,
          };
          await this.editFeature(feature, uploadDetails.data.featureType);
        }
      });

    await uploadModal.present();
  }

  async componentWillLoad() {
    this.featuresAPI = new FeaturesApi('land_use', 'lui_user');
    this.buildingCache = {};
    this.parcelCache = {};
    this.parkingLotCache = {};
    this.displayName = Cookies.get('display_name');
  }

  render() {
    return [
      <ion-menu contentId="app-root" side="end" menuId="sideMenu" swipeGesture={false}>
        <ion-header>
          <ion-toolbar>
            <ion-title>Options</ion-title>
          </ion-toolbar>
        </ion-header>
        <ion-content class="ion-padding">
          <ion-grid>
            <ion-row class="ion-align-items-center">
              <ion-col style={{ 'text-align': 'center' }}>
                <ion-button
                  class="inMenuButton"
                  fill="outline"
                  onClick={async () => {
                    await menuController.close('sideMenu');
                    this.beginUpload();
                  }}
                  disabled={this.drawing}
                >
                  <ion-label class="buttonText">Upload GeoJSON</ion-label>
                </ion-button>
              </ion-col>
            </ion-row>
            <ion-row class="ion-align-items-center">
              <ion-col style={{ 'text-align': 'center' }}>
                <ion-button
                  class="inMenuButton"
                  fill="outline"
                  onClick={async () => {
                    await menuController.close('sideMenu');
                    this.startDraw('building');
                  }}
                  disabled={this.drawing}
                >
                  <ion-label class="buttonText">Create New Building</ion-label>
                </ion-button>
              </ion-col>
            </ion-row>
            <ion-row class="ion-align-items-center">
              <ion-col style={{ 'text-align': 'center' }}>
                <ion-button
                  class="inMenuButton"
                  fill="outline"
                  onClick={async () => {
                    await menuController.close('sideMenu');
                    this.startDraw('parkingLot');
                  }}
                  disabled={this.drawing}
                >
                  <ion-label class="buttonText">Create New Parking Lot</ion-label>
                </ion-button>
              </ion-col>
            </ion-row>

            <ion-row class="ion-align-items-center">
              <ion-col style={{ 'text-align': 'center' }}>
                <ion-button class="inMenuButton" fill="outline" href="/logout">
                  <ion-label class="buttonText">Log out</ion-label>
                </ion-button>
              </ion-col>
            </ion-row>
          </ion-grid>
        </ion-content>
      </ion-menu>,
      <gl-app
        label={this.label}
        menu={true}
        menuLabel="Focus View Information"
        id="app-root"
      >
        <gl-draw-controller></gl-draw-controller>
        <ion-title slot="end-buttons">{this.displayName}</ion-title>
        <ion-menu-button
          slot="end-buttons"
          autoHide={false}
          menu="sideMenu"
        ></ion-menu-button>
        <lui-menu
          slot="menu"
          building={this.selectedBuilding}
          parcel={this.selectedParcel}
          parkingLot={this.selectedParkingLot}
        ></lui-menu>
        <gl-map
          longitude={this.long}
          latitude={this.lat}
          zoom={this.zoom}
          maxZoom={24}
          clickMode="all"
          id="mainMap"
          hash={true}
          ref={(el: HTMLGlMapElement) => (this.map = el)}
        >
          <gl-style
            id="basemap"
            url="https://maps.ccrpc.org/basemaps/hybrid-2023/style.json"
            basemap={true}
            name="Basic"
            enabled={true}
          ></gl-style>
          <lui-style
            insertIntoStyle="basemap"
            insertBeneathLayer="admin_sub"
            enabled={true}
            clickable={!this.drawing}
            building={this.selectedBuilding}
            parcel={this.selectedParcel}
            parkingLot={this.selectedParkingLot}
          ></lui-style>
        </gl-map>
        <gl-draw-toolbar slot="after-content" mapId="mainMap"></gl-draw-toolbar>
      </gl-app>,
    ];
  }

  componentDidLoad() {
    if (this.map) {
      const map = this.map.map as mapboxgl.Map;
      map.setMaxBounds([
        [-88.4909897, 39.8724889],
        [-87.9176502, 40.4140038],
      ]);
      map.setMaxPitch(0);

      // disable map rotation using right click + drag
      map.dragRotate.disable();

      // disable map rotation using touch rotation gesture
      map.touchZoomRotate.disableRotation();
    }
  }
}
