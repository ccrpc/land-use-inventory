import { newE2EPage } from '@stencil/core/testing';

describe('lui-app', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<lui-app></lui-app>');

    const element = await page.find('lui-app');
    expect(element).toHaveClass('hydrated');
  });
});
