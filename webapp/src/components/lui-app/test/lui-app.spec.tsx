import { newSpecPage } from '@stencil/core/testing';
import { BaseApp } from '../base-app';

describe('base-app', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [BaseApp],
      html: `<base-app></base-app>`,
    });
    expect(page.root).toEqualHtml(`
      <base-app>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </base-app>
    `);
  });
});
