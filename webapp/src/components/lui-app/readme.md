# base-app

This is the main page of the application and what the user sees
Other components are placed inside the app and the app also handles components throwing events
such as editing a feature.

<!-- Auto Generated Below -->

## Properties

| Property | Attribute | Description                         | Type     | Default                |
| -------- | --------- | ----------------------------------- | -------- | ---------------------- |
| `label`  | `label`   | The title of the App                | `string` | `'Land Use Inventory'` |
| `lat`    | `lat`     | Initial latitude center of the map  | `number` | `40.08079`             |
| `long`   | `long`    | Initial longitude center of the map | `number` | `-88.25211`            |
| `zoom`   | `zoom`    | How zoomed in the application is    | `number` | `15`                   |

## Dependencies

### Depends on

- ion-menu
- ion-header
- ion-toolbar
- ion-title
- ion-content
- ion-grid
- ion-row
- ion-col
- ion-button
- ion-label
- gl-app
- gl-draw-controller
- ion-menu-button
- [lui-menu](../lui-menu)
- gl-map
- gl-style
- [lui-style](../lui-style)
- gl-draw-toolbar

### Graph

```mermaid
graph TD;
  lui-app --> ion-menu
  lui-app --> ion-header
  lui-app --> ion-toolbar
  lui-app --> ion-title
  lui-app --> ion-content
  lui-app --> ion-grid
  lui-app --> ion-row
  lui-app --> ion-col
  lui-app --> ion-button
  lui-app --> ion-label
  lui-app --> gl-app
  lui-app --> gl-draw-controller
  lui-app --> ion-menu-button
  lui-app --> lui-menu
  lui-app --> gl-map
  lui-app --> gs[gl-style]
  lui-app --> ls[lui-style]
  lui-app --> gl-draw-toolbar
  ion-menu --> ion-backdrop
  ion-button --> ion-ripple-effect
  gl-app --> ion-menu
  gl-app --> ion-header
  gl-app --> ion-toolbar
  gl-app --> ion-title
  gl-app --> ion-content
  gl-app --> ion-menu-toggle
  gl-app --> ion-button
  gl-app --> ion-icon
  gl-app --> ion-buttons
  gl-app --> ion-footer
  gl-app --> ion-split-pane
  gl-app --> ion-app
  ion-menu-button --> ion-icon
  ion-menu-button --> ion-ripple-effect
  lui-menu --> ion-grid
  lui-menu --> ion-row
  lui-menu --> ion-col
  lui-menu --> ion-button
  lui-menu --> ion-content
  ls[lui-style] --> gs[gl-style]
  gl-draw-toolbar --> ion-toolbar
  gl-draw-toolbar --> ion-title
  gl-draw-toolbar --> ion-buttons
  gl-draw-toolbar --> ion-button
  gl-draw-toolbar --> ion-icon
  style lui-app fill:#f9f,stroke:#333,stroke-width:4px
```

---

_Built with [StencilJS](https://stenciljs.com/)_
