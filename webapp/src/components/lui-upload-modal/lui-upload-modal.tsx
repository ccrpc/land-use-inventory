import { Component, Host, h, Prop, Element, State } from '@stencil/core';
import {
  buildingInterface,
  parkingLotInterface,
} from '../lui-style/config/featureInterfaces';
import { ToggleChangeEventDetail } from '@ionic/core';
import { luiLog } from '../utils';
import { Polygon } from 'geojson';

export interface uploadDetails {
  success: boolean;
  geometry: Polygon;
  featureType: string;
  id: number;
}

@Component({
  tag: 'lui-upload-modal',
  styleUrl: 'lui-upload-modal.css',
  shadow: true,
})
export class LuiUploadModal {
  @Element() el: HTMLElement;
  /** Information on the currently selected Building (if there is one) */
  @Prop() selectedBuilding: buildingInterface = undefined;
  /**Information on the currently selected Parking Lot (if there is one) */
  @Prop() selectedParkingLot: parkingLotInterface = undefined;

  buildingToggle: HTMLIonToggleElement;
  parkingToggle: HTMLIonToggleElement;
  fileInput: HTMLInputElement;
  uploadDetail: uploadDetails;
  @State() errorMessage: string;
  @State() uploading: boolean = false;
  @State() progress: number = 0;
  @State() fileString: string = undefined;
  @State() fileSelected: boolean = false;
  @State() fileParsed: boolean = false;

  /** Closes the modal this component assumes it is in */
  async closeModal(result: uploadDetails): Promise<void> {
    await (this.el.closest('ion-modal') as HTMLIonModalElement).dismiss(result);
  }

  loadFile() {
    luiLog('info', 'Loading File');
    if (!this.fileInput || !this.fileInput.files) {
      luiLog('error', 'Unable to read input element');
      return;
    } else if (this.fileInput.files.length === 0) {
      luiLog('error', 'Please select a geojson file to upload');
      return;
    }

    this.uploading = true;
    this.fileParsed = false;
    this.fileString = undefined;
    const reader = new FileReader();

    reader.addEventListener('progress', this.uploadProgress.bind(this));
    reader.addEventListener('loadend', this.uploadComplete.bind(this));

    reader.readAsText(this.fileInput.files[0]);
  }

  uploadProgress(event: ProgressEvent<FileReader>) {
    if (event.loaded && event.total) {
      this.progress = (event.loaded / event.total) * 100;
    }
  }

  uploadComplete(event: ProgressEvent<FileReader>) {
    const eventReader = event.target;
    if (eventReader.error) {
      luiLog('error', eventReader.error);
    } else {
      luiLog('info', 'File loaded');
      this.fileString = eventReader.result as string;
    }
    this.uploading = false;
    eventReader.removeEventListener('progress', this.uploadProgress.bind(this));
    eventReader.removeEventListener('loadend', this.uploadComplete.bind(this));

    this.processFile();
  }

  processFile() {
    let fileJSON: any;
    let type: 'building' | 'parkingLot' = undefined;
    let id: number = undefined;
    try {
      fileJSON = this.parseFile();
      [type, id] = this.getFeatureTypeAndID();
    } catch (error) {
      luiLog('info', error);
      luiLog('error', this.errorMessage);
      return;
    }
    this.uploadDetail = {
      success: id !== undefined && type !== undefined,
      geometry: fileJSON.features[0].geometry as Polygon,
      featureType: type,
      id: id,
    };
  }

  parseFile(): any {
    luiLog('info', 'Parsing geojson file');
    let success = false;
    let fileJSON: any;
    try {
      fileJSON = JSON.parse(this.fileString);
    } catch (error) {
      if (error instanceof SyntaxError) {
        this.errorMessage = `Error parsing JSON file, invalid JSON, ${error}`;
        luiLog('info', error);
        throw error;
      } else {
        this.errorMessage = error;
        luiLog('info', error);
        throw error;
      }
    }
    if (!fileJSON.features || fileJSON.features.length == 0) {
      this.errorMessage = 'No features provided in geojson';
    } else if (fileJSON.features.length > 1) {
      this.errorMessage =
        'GeoJSON contains more than one feature, please limit the geojson to one feature only';
    } else if (!fileJSON.features[0].geometry) {
      this.errorMessage = 'Feature does not have geometry';
    } else if (fileJSON.features[0].geometry.type != 'Polygon') {
      this.errorMessage = `Feature geometry must be of type Polygon, but is ${fileJSON.features[0].geometry.type} instead.`;
    } else {
      success = true;
    }

    if (success) {
      this.fileParsed = true;
      return fileJSON;
    } else {
      throw new Error(this.errorMessage);
    }
  }

  getFeatureTypeAndID(): ['building' | 'parkingLot', number] {
    luiLog('info', 'File Parsed, checking for feature');
    let type: 'building' | 'parkingLot' = undefined;
    let id: number = undefined;

    if (this.selectedBuilding) {
      if (this.selectedParkingLot) {
        // We have both a potential parking lot and a building so we need to refer to the toggles
        if (this.buildingToggle.checked) {
          if (this.parkingToggle.checked) {
            this.errorMessage =
              'Both building and parking lot are selected simultaneously';
          } else {
            type = 'building';
            id = this.selectedBuilding.id;
          }
        } else if (this.parkingToggle.checked) {
          type = 'parkingLot';
          id = this.selectedParkingLot.id;
        } else {
          this.errorMessage =
            'Neither building nor parking lot are toggled while both are available';
        }
      } else {
        type = 'building';
        id = this.selectedBuilding.id;
      }
    } else if (this.selectedParkingLot) {
      type = 'parkingLot';
      id = this.selectedParkingLot.id;
    } else {
      this.errorMessage = 'Neither building nor Parking Lot are selected post parsing';
    }

    if (type !== undefined && id !== undefined) {
      return [type, id];
    } else {
      throw new Error(this.errorMessage);
    }
  }

  updateDatabase() {
    if (!this.fileString) {
      return;
    }
    if (!this.fileParsed) {
      this.processFile();
    }
    if (this.uploadDetail.success) {
      this.closeModal(this.uploadDetail);
    }
  }

  render() {
    let showInput: boolean = true;
    const justBuilding: HTMLIonLabelElement = (
      <ion-label>
        Changing geometry for Building with id: {this.selectedBuilding?.id}
      </ion-label>
    );

    const justParkingLot: HTMLIonLabelElement = (
      <ion-label>
        Changing geometry for Parking lot with id: {this.selectedParkingLot?.id}
      </ion-label>
    );

    const bothToggle: (HTMLIonLabelElement | HTMLIonListElement)[] = [
      <ion-label>Select which feature to change geometry for:</ion-label>,
      <ion-list>
        <ion-item>
          <ion-label>
            Change geometry for Building with id: {this.selectedBuilding?.id}
          </ion-label>
          <ion-toggle
            ref={(el) => (this.buildingToggle = el)}
            onIonChange={(event: CustomEvent<ToggleChangeEventDetail>) => {
              if (event.detail.checked) this.parkingToggle.checked = false;
            }}
            checked={true}
          ></ion-toggle>
        </ion-item>
        <ion-item>
          <ion-label>
            Change geometry for Parking Lot with id: {this.selectedParkingLot?.id}
          </ion-label>
          <ion-toggle
            ref={(el) => (this.parkingToggle = el)}
            onIonChange={(event: CustomEvent<ToggleChangeEventDetail>) => {
              if (event.detail.checked) this.buildingToggle.checked = false;
            }}
            checked={false}
          ></ion-toggle>
        </ion-item>
      </ion-list>,
    ];

    const neither: HTMLIonLabelElement = (
      <ion-label>
        To change the geometry of a building or parking lot, first select it on the map.
      </ion-label>
    );

    let selectOptions: HTMLIonLabelElement | (HTMLIonLabelElement | HTMLIonListElement)[];
    if (this.selectedBuilding) {
      if (this.selectedParkingLot) {
        selectOptions = bothToggle;
      } else {
        selectOptions = justBuilding;
      }
    } else if (this.selectedParkingLot) {
      selectOptions = justParkingLot;
    } else {
      selectOptions = neither;
      showInput = false;
    }

    return (
      <Host>
        <ion-header>
          <ion-toolbar color="secondary">
            <ion-title>Upload GeoJSON to set Geometry</ion-title>
            <ion-buttons slot="end">
              <ion-button
                fill="outline"
                onClick={() =>
                  this.closeModal({
                    success: false,
                    geometry: null,
                    featureType: null,
                    id: null,
                  })
                }
              >
                Cancel
                <ion-icon name="close"></ion-icon>
              </ion-button>
            </ion-buttons>
          </ion-toolbar>
        </ion-header>
        <div class="info">
          <ion-label class="ion-text-wrap ">
            Here one may upload a GeoJSON file to set the geometry of a building or
            parking lot. Similar to a newly created building or parking lot, a change in
            geometry may take up to 5 minutes to propagate.
            <br />
            <br />
            Three things must be true of the GeoJSON:
            <br />
            1. The Coordinate Refernce System (CRS) must be 4326 (WGS 84)
            <br />
            2. The GeoJSON file contains a single feature
            <br />
            3. The geometry of that feature is a polygon
          </ion-label>
          <br />
          <br />
          {selectOptions}
        </div>
        {showInput ? (
          <input
            type="file"
            accept=".geojson"
            id="geojson-file"
            disabled={this.uploading}
            ref={(el) => (this.fileInput = el)}
            onChange={() => {
              this.fileSelected = this.fileInput?.files?.length !== 0;
              this.fileParsed = false;
              this.fileString = undefined;
              this.uploading = false;
              this.uploadDetail = undefined;
              this.errorMessage = undefined;
            }}
          ></input>
        ) : null}
        <br />
        {this.fileSelected ? (
          <ion-button
            fill="outline"
            onClick={() => this.loadFile()}
            disabled={this.uploading}
          >
            Upload File<ion-icon name="add"></ion-icon>
          </ion-button>
        ) : null}
        <br />
        {this.uploading || this.fileString ? (
          <ion-label>Upload In progress: {this.progress}%</ion-label>
        ) : null}
        <br />
        {this.fileString ? <ion-label>Parsing file...</ion-label> : null}
        <br />
        {this.errorMessage ? (
          <ion-label>Error while processing file: {this.errorMessage}</ion-label>
        ) : null}
        <br />
        {this.fileParsed ? <ion-label>Parsing complete.</ion-label> : null}
        <br />
        {this.fileParsed ? (
          <ion-button fill="outline" onClick={() => this.updateDatabase()}>
            Upload Geometry to Database<ion-icon name="done-all"></ion-icon>
          </ion-button>
        ) : null}
      </Host>
    );
  }
}
