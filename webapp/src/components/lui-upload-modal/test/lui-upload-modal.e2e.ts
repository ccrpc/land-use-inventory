import { newE2EPage } from '@stencil/core/testing';

describe('lui-upload-modal', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<lui-upload-modal></lui-upload-modal>');

    const element = await page.find('lui-upload-modal');
    expect(element).toHaveClass('hydrated');
  });
});
