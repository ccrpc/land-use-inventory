import { newSpecPage } from '@stencil/core/testing';
import { LuiUploadModal } from '../lui-upload-modal';

describe('lui-upload-modal', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [LuiUploadModal],
      html: `<lui-upload-modal></lui-upload-modal>`,
    });
    expect(page.root).toEqualHtml(`
      <lui-upload-modal>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </lui-upload-modal>
    `);
  });
});
