import { Component, Element, h, Host, Prop, State } from '@stencil/core';
import {
  featureInterface,
  parcelInterface,
  buildingInterface,
  parkingLotInterface,
} from '../lui-style/config/featureInterfaces';
import { InputChangeEventDetail } from '@ionic/core';
import { isObjectEmpty, featureTitles, luiLog } from '../utils';

@Component({
  tag: 'lui-edit-modal',
  styleUrl: 'lui-edit-modal.css',
  shadow: true,
})
export class LuiEditModal {
  @Element() el: HTMLElement;
  /** The original feature being edited */
  @Prop() feature: featureInterface;
  /** Edits made to the feature */
  @State() editedFeature: featureInterface = undefined;

  /** Closes the modal this component assumes it is in */
  async closeModal(): Promise<void> {
    await (this.el.closest('ion-modal') as HTMLIonModalElement).dismiss();
  }

  /** Rather than just closing the modal this submits information back to whichever entity created it */
  async submitModal(event: Event): Promise<void> {
    event.preventDefault();

    let stripped: featureInterface = this.removeNonEdits(
      this.editedFeature,
      this.feature
    );

    if (!isObjectEmpty(stripped)) {
      stripped.id = this.feature.id;
      stripped.featureType = this.feature.featureType;

      await (this.el.closest('ion-modal') as HTMLIonModalElement).dismiss({
        type: stripped.featureType,
        feature: stripped,
      });
    }
  }

  /** If the editedFeature is identical to the original feature then no changes need to be made.
   * In general this removes any instances where the two are the same which will reduce the complexity
   * of the sql call
   */
  private removeNonEdits(
    feature: featureInterface,
    compare: featureInterface
  ): featureInterface {
    if (!feature) {
      return undefined;
    }

    let featureCopy: featureInterface = { ...feature };

    if (featureCopy.featureType === 'parkingLot') {
      if (featureCopy['estimate'] !== undefined) {
        switch (featureCopy['estimate']) {
          case 'true':
          case 'True':
            featureCopy['estimate'] = true;
            break;
          case 'false':
          case 'False':
            featureCopy['estimate'] = false;
            break;
          default:
            luiLog(
              'error',
              `Invalid feature estimate value (should be [tT]rue or [fF]alse): ${featureCopy['estimate']} `
            );
            luiLog('info', 'Feature with invalid estimate:');
            luiLog('info', featureCopy);
            delete featureCopy['estimate'];
            break;
        }
      }
    }

    for (let propName in featureCopy) {
      if ((featureCopy as Object).hasOwnProperty(propName)) {
        if (featureCopy[propName] === compare[propName]) {
          delete featureCopy[propName];
        }
      }
    }

    if (compare && isObjectEmpty(featureCopy)) {
      luiLog(
        'error',
        'Attempting to edit feature but no differences were found between edit and the original'
      );
      luiLog('info', 'Two features with no differences');
      luiLog('info', compare);
      luiLog('info', feature);
    }

    return featureCopy;
  }

  /** When the user changes a value we record it and make sure to update editedFeature
   * If a value is deleted all of the way it is also completely deleted from editedFeature
   * This causes less changes and also takes care of some unexpected behavior of a value
   * persisting after it had been deleted.
   */
  private updateValue(
    event: CustomEvent<InputChangeEventDetail>,
    attributeName: string,
    invalidText: string = 'Unknown Error'
  ): void {
    let ionInput: HTMLIonInputElement = event.target as HTMLIonInputElement;
    let value: string | number = ionInput.value;

    if (value === '') {
      if (this.editedFeature.hasOwnProperty(attributeName)) {
        delete this.editedFeature[attributeName];
      }
      return;
    }

    if (typeof this.feature[attributeName] === 'number') {
      value = Number(value);
    }
    this.editedFeature[attributeName] = value;

    // Injecting validity logic here for now.
    // TODO: Put this in a place that makes more sense
    ionInput.getInputElement().then((input: HTMLInputElement) => {
      if (!input.oninvalid || !input.oninput) {
        input.oninvalid = () => input.setCustomValidity(invalidText);
        input.oninput = () => input.setCustomValidity('');
      }
    });
  }

  componentWillLoad() {
    if (this.editedFeature === undefined) {
      this.editedFeature = {
        id: this.feature.id,
        featureType: this.feature.featureType,
      };
    }
  }

  render() {
    let id: number = 0;
    let title: string = 'Unknown';
    if (!this.feature) {
      title = 'None';
    } else {
      id = this.feature.id;
      title = featureTitles[this.feature.featureType];
    }

    return (
      <Host>
        <form
          style={{ height: '100%' }}
          onSubmit={(event: Event) => this.submitModal(event)}
        >
          <ion-header>
            <ion-toolbar color="secondary">
              <ion-title>
                Editing {title} {id}
              </ion-title>
              <ion-buttons slot="end">
                <ion-button type="submit" fill="outline">
                  Submit
                  <ion-icon name="done-all"></ion-icon>
                </ion-button>
                <ion-button fill="outline" onClick={() => this.closeModal()}>
                  Cancel
                  <ion-icon name="close"></ion-icon>
                </ion-button>
              </ion-buttons>
            </ion-toolbar>
          </ion-header>
          <ion-content>{this.featureGrid()}</ion-content>
        </form>
      </Host>
    );
  }

  private featureGrid() {
    if (!this.feature) {
      return <ion-label class="header">No Feature Selected</ion-label>;
    }
    switch (this.feature.featureType) {
      case 'parcel':
        return this.editParcelGrid(this.feature as parcelInterface);
      case 'building':
        return this.editBuildingGrid(this.feature as buildingInterface);
      case 'parkingLot':
        return this.editParkingLotGrid(this.feature as parkingLotInterface);
      default:
        luiLog(
          'info',
          'attempting to display feature grid of feature without an editModalGrid'
        );
        luiLog('info', this.feature);
        return (
          <ion-grid>
            <ion-row>
              <ion-col class="error">
                <ion-label class="header">
                  Feature of Unknown Type: {this.feature.featureType}
                </ion-label>
              </ion-col>
            </ion-row>
          </ion-grid>
        );
    }
  }
  /**
   * Creates the JSX for the column to display for this attribute, used in the parcelGrid and buildingGrid below.
   * This functions uses this.feature as the original feature, and this.editFeature for the current one.
   * @param {string} title - The title to display over the attribute
   * @param {string} attributeName - The name of the attribute in the features interface. For example 'lbcs_site' or 'aprop'.
   * @param {string} [type=text] - The type to use for form validation, currently supported are 'text', 'number', and 'array'.
   * @param {number} [size=6] - The size of the column using ion-col's system. This variable can range between 1 and 12 where 12 is the full width of the row.
   * @param {string} [alternative=Unknown] - What to display if attribute does not exist on our original object
   * @returns The JSX to display this column
   */
  private editAttributeCol(
    title: string,
    attributeName: string,
    type: string = 'text',
    size: number = 6,
    alternative: string = 'Unknown'
  ) {
    let attribute = this.feature[attributeName];
    let value = this.editedFeature[attributeName];

    if (attribute && (typeof attribute === 'string' || attribute instanceof String)) {
      attribute = attribute.replace(/,(?=[^\s])/g, ', ');
    }
    if (value && (typeof value === 'string' || value instanceof String)) {
      value = value.replace(/,(?=[^\s])/g, ', ');
    }

    let invalidText: string;
    let validPattern: string = '.*';
    if (type === 'number') {
      invalidText = 'Enter a number';
      validPattern = '[0-9]*';
    } else if (type === 'array') {
      invalidText = "Enter a comma separated list of numbers inside '{' and '}'";
      validPattern = '\\{[0-9]*(, *[0-9]+)*\\}'; // We have to have the \\ or it will be discarded when it is assigned below.
    } else if (type === 'boolean') {
      invalidText = 'Enter either true, True, false, or False';
      validPattern = '([tT]rue|[fF]alse)';
    } else {
      invalidText = 'Unknown Error';
      validPattern = '.*';
    }

    let input = (
      <ion-input
        placeholder={
          attribute !== undefined && attribute !== null
            ? (attribute as string)
            : alternative
        }
        inputMode={type === 'number' ? 'numeric' : 'text'}
        type="text"
        value={value !== undefined && value !== null ? (value as string) : ''}
        onIonChange={(event) => this.updateValue(event, attributeName, invalidText)}
        pattern={validPattern}
        title={title}
        name={attributeName}
      />
    );

    return (
      <ion-col size={size.toString()} class="attributes">
        <p>{title}</p>
        {input}
      </ion-col>
    );
  }

  /**
   * Creates the JSX ion-grid to display a parcel's information
   * @param parcel - The parcel whose information will be displayed
   * @returns JSX for the ion grid that displays the parcel's information
   */
  private editParcelGrid(parcel: parcelInterface) {
    if (!parcel) {
      return <p style={{ 'text-align': 'center' }}>No Parcel Selected</p>;
    }
    return (
      <ion-grid>
        <ion-row>
          {this.editAttributeCol('LBCS Site', 'lbcs_site', 'array')}
          {this.editAttributeCol('LBCS Activity', 'lbcs_activity', 'array')}
        </ion-row>
        <ion-row>
          {this.editAttributeCol('APROP Land Use Code', 'aprop', 'number')}
          {this.editAttributeCol('Buildings', 'buildings', 'array')}
        </ion-row>
        <ion-row>
          {this.editAttributeCol(
            'Impervious Area (in sq feet)',
            'impervious_area',
            'number'
          )}
          {this.editAttributeCol('Parcel Pin', 'pin', 'number')}
        </ion-row>
        <ion-row>
          {this.editAttributeCol('Comment', 'external_comment', 'string', 12)}
        </ion-row>
      </ion-grid>
    );
  }

  /**
   * Create the JSX ion-grid to display a building's information.
   * @param building - The building whose information to display
   * @returns JSX for the ion grid to display the building's information
   */
  private editBuildingGrid(building: buildingInterface) {
    if (!building) {
      return <p style={{ 'text-align': 'center' }}>No Building Selected</p>;
    }
    return (
      <ion-grid>
        <ion-row>
          {this.editAttributeCol('Street Name', 'street_name', 'text')}
          {this.editAttributeCol('LBCS Structure', 'lbcs_structure', 'array')}
        </ion-row>
        <ion-row>
          {this.editAttributeCol('Stories', 'stories', 'number')}
          {this.editAttributeCol('Units', 'units', 'number')}
        </ion-row>
        <ion-row>
          {this.editAttributeCol(
            "Containing Parcel's Pin",
            'parcel_pin',
            'array',
            12,
            'None'
          )}
        </ion-row>
        <ion-row>
          {this.editAttributeCol('Left Setback', 'left_setback', 'number')}
          {this.editAttributeCol('Right Setback', 'right_setback', 'number')}
        </ion-row>
        <ion-row>
          {this.editAttributeCol('Front Setback', 'front_setback', 'number')}
          {this.editAttributeCol('Rear Setback', 'rear_setback', 'number')}
        </ion-row>
        <ion-row>
          {this.editAttributeCol('Comment', 'external_comment', 'string', 12)}
        </ion-row>
      </ion-grid>
    );
  }

  private editParkingLotGrid(parkingLot: parkingLotInterface) {
    if (!parkingLot) {
      return <p style={{ 'text-align': 'center' }}>No Parking Lot Selected</p>;
    }

    return (
      <ion-grid>
        <ion-row>
          {this.editAttributeCol('Parking Spaces', 'parking_spaces', 'number')}
          {this.editAttributeCol('Is this an estimate?', 'estimate', 'boolean')}
        </ion-row>
        <ion-row>
          {this.editAttributeCol('Area (in sq feet)', 'area_sq_ft', 'number', 12)}
        </ion-row>
        <ion-row>
          {this.editAttributeCol('Comment', 'external_comment', 'string', 12)}
        </ion-row>
      </ion-grid>
    );
  }
}
