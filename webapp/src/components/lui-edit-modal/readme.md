# lui-feature-edit-modal



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description                       | Type               | Default     |
| --------- | --------- | --------------------------------- | ------------------ | ----------- |
| `feature` | --        | The original feature being edited | `featureInterface` | `undefined` |


## Dependencies

### Depends on

- ion-header
- ion-toolbar
- ion-title
- ion-buttons
- ion-button
- ion-icon
- ion-content
- ion-label
- ion-grid
- ion-row
- ion-col
- ion-input

### Graph
```mermaid
graph TD;
  lui-edit-modal --> ion-header
  lui-edit-modal --> ion-toolbar
  lui-edit-modal --> ion-title
  lui-edit-modal --> ion-buttons
  lui-edit-modal --> ion-button
  lui-edit-modal --> ion-icon
  lui-edit-modal --> ion-content
  lui-edit-modal --> ion-label
  lui-edit-modal --> ion-grid
  lui-edit-modal --> ion-row
  lui-edit-modal --> ion-col
  lui-edit-modal --> ion-input
  ion-button --> ion-ripple-effect
  style lui-edit-modal fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
