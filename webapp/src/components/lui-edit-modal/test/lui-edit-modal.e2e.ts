import { newE2EPage } from '@stencil/core/testing';

describe('lui-edit-modal', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<lui-edit-modal></lui-edit-modal>');

    const element = await page.find('lui-edit-modal');
    expect(element).toHaveClass('hydrated');
  });
});
