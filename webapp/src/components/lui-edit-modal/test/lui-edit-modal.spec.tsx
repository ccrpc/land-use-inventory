import { newSpecPage } from '@stencil/core/testing';
import { LuiEditModal } from '../lui-edit-modal';

describe('lui-edit-modal', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [LuiEditModal],
      html: `<lui-edit-modal></lui-edit-modal>`,
    });
    expect(page.root).toEqualHtml(`
      <lui-edit-modal>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </lui-edit-modal>
    `);
  });
});
