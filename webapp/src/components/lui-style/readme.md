# lui-style

The layers, and via what sources, to display in the interactive map.

<!-- Auto Generated Below -->

## Properties

| Property             | Attribute              | Description                                                      | Type                  | Default     |
| -------------------- | ---------------------- | ---------------------------------------------------------------- | --------------------- | ----------- |
| `building`           | --                     | Information on the currently selected building (if there is one) | `buildingInterface`   | `undefined` |
| `clickable`          | `clickable`            | Whether or not clicking is enabled on the map                    | `boolean`             | `true`      |
| `enabled`            | `enabled`              | Whether the lui styles are enabled                               | `boolean`             | `true`      |
| `insertBeneathLayer` | `insert-beneath-layer` | What layer to insert the lui styles beneath                      | `string`              | `undefined` |
| `insertIntoStyle`    | `insert-into-style`    | What gl-style to insert the lui styles into                      | `string`              | `undefined` |
| `parcel`             | --                     | Information on the currently selected parcel (if there is one)   | `parcelInterface`     | `undefined` |
| `parkingLot`         | --                     | Information on the currently selected parking lot                | `parkingLotInterface` | `undefined` |

## Dependencies

### Used by

- [lui-app](../lui-app)

### Depends on

- gl-style

### Graph

```mermaid
graph TD;
  ls[lui-style] --> ga[gl-style]
  lui-app --> ls[lui-style]
  style ls fill:#f9f,stroke:#333,stroke-width:4px
```

---

_Built with [StencilJS](https://stenciljs.com/)_
