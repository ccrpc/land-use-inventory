import { Component, Prop, h, Host } from '@stencil/core';
import { staticLayers } from './config/staticLayers';
import { staticSources } from './config/staticSources';
import {
  buildingInterface,
  parcelInterface,
  parkingLotInterface,
} from './config/featureInterfaces';
type buildingLayerType = typeof staticLayers.buildings;
type parcelLayerType = typeof staticLayers.parcels;
type parkingLotLayerType = typeof staticLayers.parking_lots;

@Component({
  tag: 'lui-style',
  styleUrl: 'lui-style.css',
  shadow: false,
})
export class LuiStyle {
  /** What gl-style to insert the lui styles into */
  @Prop() insertIntoStyle: string = undefined;
  /** What layer to insert the lui styles beneath */
  @Prop() insertBeneathLayer: string = undefined;
  /** Whether the lui styles are enabled */
  @Prop() enabled: boolean = true;
  /** Information on the currently selected building (if there is one) */
  @Prop() building: buildingInterface = undefined;
  /** Information on the currently selected parcel (if there is one) */
  @Prop() parcel: parcelInterface = undefined;
  /** Information on the currently selected parking lot  */
  @Prop() parkingLot: parkingLotInterface = undefined;
  /** Whether or not clicking is enabled on the map */
  @Prop() clickable: boolean = true;

  private updateSelectedFeatures(
    buildingLayerJson: buildingLayerType,
    parcelLayerJson: parcelLayerType,
    parkingLotLayerJson: parkingLotLayerType
  ): void {
    let buildingLine = buildingLayerJson.find((i) => i.id === 'building-line');
    if (buildingLine) {
      const selectedBuilding: number = this.building?.id
        ? this.building.id
        : -1;
      buildingLine.paint['line-color'] = [
        'let',
        'selected',
        selectedBuilding,
        ...buildingLine.paint['line-color'].slice(3),
      ];
    }

    let parcelLine = parcelLayerJson.find((i) => i.id === 'parcel-line');
    if (parcelLine) {
      const selectedParcel: number = this.parcel?.id ? this.parcel.id : -1;
      parcelLine.paint['line-color'] = [
        'let',
        'selected',
        selectedParcel,
        ...parcelLine.paint['line-color'].slice(3),
      ];

      parcelLine.paint['line-width'] = [
        'let',
        'selected',
        selectedParcel,
        ...parcelLine.paint['line-width'].slice(3),
      ];
    }

    let parkingLotLine = parkingLotLayerJson.find((i) => i.id === 'parking_lot-line');
    if (parkingLotLine) {
      const selectedParkingLot: number = this.parkingLot?.id ? this.parkingLot.id : -1;
      parkingLotLine.paint['line-color'] = [
        'let',
        'selected',
        selectedParkingLot,
        ...parkingLotLine.paint['line-color'].slice(3),
      ];
    }
  }

  render() {
    const baseJson = {
      version: 8,
      glyphs: '/trex/fonts/{fontstack}/{range}.pbf',
      metadata: {
        'mapbox:autocomposite': false,
        'maputnik:renderer': 'mbgljs',
      },
      name: 't-rex',
    };

    let parcelJson = {
      ...baseJson,
      layers: staticLayers.parcels,
      sources: staticSources.parcels,
    };
    let buildingJson = {
      ...baseJson,
      layers: staticLayers.buildings,
      sources: staticSources.buildings,
    };
    let parkingJson = {
      ...baseJson,
      layers: staticLayers.parking_lots,
      sources: staticSources.parking_lots,
    };

    this.updateSelectedFeatures(
      buildingJson.layers,
      parcelJson.layers,
      parkingJson.layers
    );

    return (
      <Host>
        <gl-style
          id="building"
          name="Buildings"
          clickable-layers={this.clickable ? 'building-fill' : null}
          json={buildingJson}
          enabled={this.enabled}
          insertIntoStyle={this.insertIntoStyle}
          insertBeneathLayer={this.insertBeneathLayer}
          basemap={false}
        ></gl-style>
        <gl-style
          id="parking_lot"
          name="ParkingLots"
          clickable-layers={this.clickable ? 'parking_lot-fill' : null}
          json={parkingJson}
          enabled={this.enabled}
          insertIntoStyle={this.insertIntoStyle}
          insertBeneathLayer="building"
          basemap={false}
        ></gl-style>
        <gl-style
          id="parcel"
          name="Parcels"
          clickable-layers={this.clickable ? 'parcel-fill' : null}
          json={parcelJson}
          enabled={this.enabled}
          insertIntoStyle={this.insertIntoStyle}
          insertBeneathLayer="parking_lots"
          basemap={false}
        ></gl-style>
      </Host>
    );
  }
}
