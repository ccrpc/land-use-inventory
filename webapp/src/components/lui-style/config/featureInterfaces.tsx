export interface featureInterface {
  geometry?: {
    type: string;
    coordinates: number[][][];
  };
  id: number;
  featureType: string;
  date?: string;
  last_edited?: string;
  external_comment?: string;
}

export interface parcelInterface extends featureInterface {
  aprop?: number;
  image_url?: string;
  lbcs_activity?: string;
  lbcs_site?: string;
  pin?: string;
  buildings?: string;
  impervious_area?: string;
}

export interface buildingInterface extends featureInterface {
  // building_id: number; (this is replaced with id when I get the feature either from trex or the features_api)
  parcel_pin?: string;
  image_url?: string;
  lbcs_structure?: string;
  front_setback?: number;
  left_setback?: number;
  rear_setback?: number;
  right_setback?: number;
  stories?: number;
  street_name?: string;
  units?: number;
  study_area?: boolean;
}

export interface parkingLotInterface extends featureInterface {
  parking_spaces?: number;
  area_sq_ft?: number;
  parcel_id?: string;
  estimate?: boolean;
}
