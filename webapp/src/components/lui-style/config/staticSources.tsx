export const staticSources = {
  buildings: {
    building: {
      type: 'vector',
      url: '/trex/building.json',
      minzoom: 14,
    },
  },
  parcels: {
    parcel: {
      type: 'vector',
      url: '/trex/parcel.json',
      minzoom: 12,
    },
  },
  parking_lots: {
    parking_lot: {
      type: 'vector',
      url: '/trex/parking_lot.json',
      minzoom: 14,
    },
  },
  // addresses: {
  //   address_database_view: {
  //     type: 'vector',
  //     url: 'http://localhost:6767/address_database_view.json',
  //   }
  // },
};
