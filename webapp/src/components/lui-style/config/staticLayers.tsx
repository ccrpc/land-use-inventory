export const staticLayers = {
  parcels: [
    {
      'id': 'parcel-fill',
      'source': 'parcel',
      'source-layer': 'parcel',
      'type': 'fill',
      'paint': {
        'fill-color': 'rgba(85,170,92,0)',
      },
      'layout': {
        'fill-sort-key': 0,
      },
    },
    {
      'id': 'parcel-line',
      'source': 'parcel',
      'source-layer': 'parcel',
      'type': 'line',
      'paint': {
        'line-color': [
          'let',
          'selected',
          -1,
          [
            'case',
            ['boolean', ['==', ['get', 'id'], ['var', 'selected']], false],
            '#c32727',
            '#55aa5c',
          ],
        ],
        'line-width': [
          'let',
          'selected',
          -1,
          [
            'case',
            ['boolean', ['==', ['get', 'id'], ['var', 'selected']], false],
            1.25,
            1,
          ],
        ],
        'line-dasharray': [2, 3],
      },
      'layout': {
        'line-sort-key': 1,
      },
    },
  ],
  buildings: [
    {
      'id': 'building-fill',
      'source': 'building',
      'source-layer': 'building',
      'type': 'fill',
      'paint': {
        'fill-color': 'rgba(70,243,47,0)',
      },
      'layout': {
        'fill-sort-key': 0,
      },
    },
    {
      'id': 'building-line',
      'source': 'building',
      'source-layer': 'building',
      'type': 'line',
      'paint': {
        'line-color': [
          'let',
          'selected',
          -1,
          [
            'case',
            ['boolean', ['==', ['get', 'building_id'], ['var', 'selected']], false],
            '#c32727',
            '#46f32f',
          ],
        ],
        'line-width': 1.25,
      },
      'layout': {
        'line-sort-key': 1,
      },
    },
  ],
  parking_lots: [
    {
      'id': 'parking_lot-fill',
      'source': 'parking_lot',
      'source-layer': 'parking_lot',
      'type': 'fill',
      'paint': {
        'fill-color': 'rgba(70,243,47,0)',
      },
      'layout': {
        'fill-sort-key': 0,
      },
    },
    {
      'id': 'parking_lot-line',
      'source': 'parking_lot',
      'source-layer': 'parking_lot',
      'type': 'line',
      'paint': {
        'line-color': [
          'let',
          'selected',
          -1,
          [
            'case',
            ['boolean', ['==', ['get', 'id'], ['var', 'selected']], false],
            '#c32727',
            '#1fe4ee',
          ],
        ],
        'line-width': 2,
        'line-dasharray': [1, 1],
      },
      'layout': {
        'line-sort-key': 1,
      },
    },
  ],
  // addresses: [
  //   {
  //     'id': 'address_database_view',
  //     'source': 'address_database_view',
  //     'source-layer': 'address_database_view',
  //     'type': 'circle',
  //     'paint': {
  //       'circle-color': '#44d03c',
  //       'circle-radius': 3,
  //     },
  //   },
  // ],
};
