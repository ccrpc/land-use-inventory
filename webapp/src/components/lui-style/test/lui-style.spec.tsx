import { newSpecPage } from '@stencil/core/testing';
import { LuiStyle } from '../lui-style';

describe('lui-style', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [LuiStyle],
      html: `<lui-style></lui-style>`,
    });
    expect(page.root).toEqualHtml(`
      <lui-style>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </lui-style>
    `);
  });
});
