import { newE2EPage } from '@stencil/core/testing';

describe('lui-style', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<lui-style></lui-style>');

    const element = await page.find('lui-style');
    expect(element).toHaveClass('hydrated');
  });
});
